<?php

use App\Tld;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('TldTableSeeder');
		$this->command->info('Seeded the top level domains!');
		$this->call('UserTableSeeder');
		$this->command->info('Seeded the user(s)!');
		$this->call('CountriesSeeder');
		$this->command->info('Seeded the countries!');
	}

}

class TldTableSeeder extends Seeder {

	public function run()
	{
		DB::table('tlds')->delete();

		Tld::create(['tld' => 'tt','cost_us' => '600', 'cost_tt' => '2000', 'maintenance_tt' => '2000', 'maintenance_us' => '600', 'cost_tt_single' => '6000', 'cost_us_single' => '2000']);

		Tld::create(['tld' => 'co.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'com.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'org.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'net.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'biz.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'pro.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'info.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
		Tld::create(['tld' => 'name.tt','cost_us' => '100', 'cost_tt' => '300', 'maintenance_tt' => '300', 'maintenance_us' => '100']);
	}

}

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();
		User::create(['name' => 'Charlton De Souza', 'email' => 'charltondesouza@gmail.com', 'password' => bcrypt('password'), 'isAdmin' => true]);


	}

}

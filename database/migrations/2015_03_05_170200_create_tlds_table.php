<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tlds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('tld')->unique();
			$table->double('cost_us');
			$table->double('cost_us_single')->nullable();
			$table->double('maintenance_us');
			$table->double('maintenance_tt');
			$table->double('cost_tt');
			$table->double('cost_tt_single')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tlds');
	}

}

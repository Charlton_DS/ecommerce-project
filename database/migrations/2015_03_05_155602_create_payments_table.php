<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->nullable();
			$table->string('invoice')->nullable();
			$table->double('amount_tt');
			$table->double('amount_us');
			$table->integer('user_id')->unsigned();
			$table->boolean('paid')->default(false);
			$table->timestamps();
			//$table->integer('cart_id')->unsigned();
			//$table->integer('domain_id')->unsigned();


			$table->foreign('user_id')
				->references('id')
				->on('users');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}

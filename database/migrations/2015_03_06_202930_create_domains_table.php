<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('tld');
			$table->string('registrant');
			$table->string('registrant_addr');
			$table->string('isLocal');
			$table->string('country');
			$table->string('dns_hosts')->nullable();
			$table->string('dns_ips')->nullable();
			$table->string('dsrecord')->nullable();
			$table->string('admin');
			$table->string('billing');
			$table->string('tech');
			$table->integer('payment_id')->unsigned();
			$table->boolean('activated')->default(false);
			$table->date('expire');
			$table->timestamps();


			$table->foreign('admin')
				->references('email')
				->on('users');

			$table->foreign('billing')
				->references('email')
				->on('users');

			$table->foreign('tech')
				->references('email')
				->on('users');

			$table->foreign('tld')
				->references('tld')
				->on('tlds');

			$table->foreign('payment_id')
				->references('id')
				->on('payments');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}

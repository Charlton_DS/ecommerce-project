<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('user');
			$table->string('billing');
			$table->string('technical');
			$table->string('dns_hosts')->nullable();
			$table->string('dns_ips')->nullable();
			$table->string('dsrecord')->nullable();
			$table->string('name');
			$table->string('tld');
			$table->boolean('approved')->nullable();
			$table->boolean('submitted');
			$table->boolean('local')->default(false);
			$table->timestamps();


			$table->foreign('user')
				->references('email')
				->on('users');

			$table->foreign('billing')
				->references('email')
				->on('users');

			$table->foreign('technical')
				->references('email')
				->on('users');

			$table->foreign('tld')
				->references('tld')
				->on('tlds');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carts');
	}

}

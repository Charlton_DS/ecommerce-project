<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class ProfileController extends Controller {


	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('profile.profile')->with('user',Auth::user());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		abort(404);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		abort(404);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		abort(404);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('profile.edit')->with('user',Auth::user());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$v = Validator::make(Request::all(), [
			'phone' 	=> 	'required|numeric',
			'country' 	=> 	'required|alpha|max:2',
			'name'		=>	'required|string',
			'address'	=>	'required:string'
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v);
		}
		$phoneUtil = PhoneNumberUtil::getInstance();
		$phone = $phoneUtil->parse(Request::input('phone'), Request::input('country'));

		if(!$phoneUtil->isValidNumberForRegion($phone,Request::input('country'))){
			return redirect()->back()->withErrors(['phone' => 'Phone number is not valid for country',]);
		}

		Auth::user()->name = Request::input('name');
		Auth::user()->phone = Request::input('phone');
		Auth::user()->country = Request::input('country');
		Auth::user()->address = Request::input('address');
		Auth::user()->save();

		return redirect(action('HomeController@index'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		abort(404);
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\VerifySfaRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;
use PragmaRX\Google2FA\Google2FA;

class SecondFactorController extends Controller {

	//
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Adds Google Second Factor Secret to user
     */
    public function enable(){
        $user = Auth::user();
        $google2fa = new Google2FA();

        $secret2fa = $google2fa->generateSecretKey();
        //$user->save();
        return response()
            ->view('secondfactor.qrcode', ['url' => $google2fa->getQRCodeGoogleUrl('tcw.tt',$user->email,$secret2fa)])
            ->withCookie(cookie('token', $secret2fa, 15));
        //return view('secondfactor.qrcode')->with('url', $google2fa->getQRCodeGoogleUrl('nic.tt',$user->email,$user->secret2fa));
    }

    public function verify (VerifySfaRequest $request){
        $user = Auth::user();
        $google2fa = new Google2FA();

        $secret = $request->secret;
        if($google2fa->verifyKey(Request::cookie('token'),$secret)){
            $user->secret2fa = Request::cookie('token');
            $cookie = Cookie::forget('token');
            $user->save();
            return redirect(action('HomeController@index'))->withCookie($cookie);
        }
        return redirect()->back()->withErrors(['token' => 'Token did not match, scan code and try again',]);
        //return view('secondfactor.qrcode')->with('url', $google2fa->getQRCodeGoogleUrl('nic.tt',$user->email,$user->secret2fa))->with('valid',$valid);
    }

}

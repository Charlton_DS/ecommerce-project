<?php namespace App\Http\Controllers;


use App\Domain;
use App\Http\Controllers\Controller;

use App\Http\Requests\RegisterDomainRequest;
use App\Http\Requests\ShowDomainRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class DomainsController
 *
 * This controller handles the manipulation of domain objects
 * @package App\Http\Controllers
 */
class DomainsController extends Controller {


	public function __construct(){
		$this->middleware('auth', ['only' => 'create']);
	}

	/**
	 * Display a listing all domains.
	 *
	 * @return Response
	 */
	public function index()
	{
		abort(404);
		$domains = Domain::all();
		return view('domains.index')->with('domains', $domains);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		abort(404);
		return view('domains.create');
		//return 'test';
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Requests\RegisterDomainRequest $request
	 * @return Response
	 */
	public function store(RegisterDomainRequest $request)
	{
		abort(404);
		Auth::user()->email;
		$domain = new Domain;

		$domain->name = $request->name;
		$domain->tld = $request->tld;
		$domain->admin = Auth::user()->email;
		$domain->save();

		Mail::send('emails.request', $domain->ToArray(), function($message)
		{
			$message->to(env('ADMIN_EMAIL'), 'TTNIC Admin')->subject('New Domain Approval');
		});

		return redirect('domain');

	}

	/**
	 * Display the specified domain.
	 *
	 * @param RegisterDomainRequest $request
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		$v = Validator::make(['id' => $id], [
			'id' => 'required|numeric',
		]);

		if ($v->fails())
		{
			return redirect()->back();
		}

		$domain = Domain::findOrFail($id);
		$hosts = explode(PHP_EOL,$domain->dns_hosts);
		$ips  = explode(PHP_EOL,$domain->dns_ips);
		return view('domains.domain')->with('domain',$domain)->with('hosts', $hosts)->with('ips',$ips);
	}

	/**
	 * Show the form for editing the specified domain.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$v = Validator::make(['id' => $id], [
			'id' => 'required|numeric',
		]);

		if ($v->fails())
		{
			return redirect()->back();
		}
		$domain = Domain::findOrFail($id);
		if($domain->owner->id != Auth::user()->id && $domain->billing->id != Auth::user()->id ){
			abort(403);
		}

		if($domain->owner->id == Auth::user()->id)
		{
			return view('domains.edit')->with('domain',$domain)->with('admin',true);
		}
		return view('domains.edit')->with('domain',$domain);
	}

	/**
	 * Update the specified domain in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$v = Validator::make(Request::all(), [
			'billing' 	=> 'required|email',
			'technical'	=> 'required|email',
			'hosts'		=> 'required|string',
			'ips'		=> 'required|string',
			'addr'		=> 'required|string',
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v);;
		}

		$domain = Domain::findOrFail($id);

		if($domain->billing != Request::input('billing') ){
			$billing = User::where('email', Request::input('billing'))->get();
			if($billing != null){
				$domain->billing = Request::input('billing');
			}

		}

		if($domain->tech != Request::input('technical') ){
			$technical = User::where('email', Request::input('technical'))->get();
			if($technical != null){
				$domain->tech = Request::input('technical');
			}

		}

		$domain->dns_hosts = str_replace(' ','',Request::input('hosts'));
		$domain->dns_ips = str_replace(' ','',Request::input('ips'));
		$domain->registrant_addr = Request::input('addr');
		$domain->save();
		return redirect(action('HomeController@index'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		abort(404);
	}

}

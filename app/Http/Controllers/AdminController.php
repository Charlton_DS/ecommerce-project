<?php namespace App\Http\Controllers;

use App\Cart;
use App\Domain;
use App\Http\Controllers\Controller;
use App\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Webpatser\Countries\Countries;

/**
 * Class AdminController
 *
 * This Controller is used for providing administration functionanilty
 * for the application
 *
 * @package App\Http\Controllers
 */
class AdminController extends Controller {


	/**
	 * Assigns Authentication and Admin middleware
	 */
	public function __construct(){
		$this->middleware('auth');
		$this->middleware('admin');
	}
	/**
	 * R$eturn the view for the administration homepage
	 *
	 * @return Response
	 */
	public function index()
	{
		$chart = $this->charts();
		return view('admin.home')->with('chart',$chart);
	}


	/**
	 * Approves submitted domains
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function approve()
	{
		//Gets form input from request
		$approved = Request::input('approved');
		$local = Request::input('local');

		//Gets the billing and admin emails for the domains being approved
		$billing_emails = Cart::distinct()->select('billing')->submitted()->notApproved()->orderBy('user')->get();
		$user_emails = Cart::distinct()->select('user')->submitted()->notApproved()->orderBy('user')->get();

		//Updates Cart Items
		foreach($approved as $request) {
			$domain = Cart::findorFail($request['id']);
			if(!$request['approved']){
				$domain->delete();
			}
			else {
				$domain->approved = $request['approved'];
				$domain->local = $request['local'];
				$domain->save();
			}
		}

		//Sends email to admin contact
		foreach($user_emails as $email){
			$input['size'] = Cart::where('user',$email->user)->payable()->count();
			$mail = $email->user;

			Mail::queue('emails.approval', $input, function($message) use ($mail)
			{
				$message->to($mail, 'TTNIC Admin')->subject('Domain Approval');
			});

		}

		//Creates payment and domains for approved domains and notifies billing
		foreach($billing_emails as $email) {
			$approved = Cart::where('billing', $email->billing)->payable()->get();
			$invoice['size'] = count($approved);



			if (count($approved) > 0) {
				//Create payment
				$payment = Payment::create(array(
					'type' => "Domain Registration",
					'amount_tt' => $this->total($approved, true),
					'amount_us' => $this->total($approved, false),
					'user_id' => $approved->first()->customerBilling->id
				));

				$payment_id = $payment->id;
				$payment->invoice = 'TT' . Carbon::now()->year . str_pad($payment_id, 5, "0", STR_PAD_LEFT);
				$payment->paid = false;
				$payment->save();

				//Storing invoice info
				$invoice['number'] = $payment->invoice;
				$invoice['amount_tt'] = $payment->amount_tt;
				$invoice['amount_us'] = $payment->amount_us;
				$invoice['name'] = $approved->first()->customerBilling->name;

				$mail = $email->billing;
				$domains = array();
				//Creates domains
				$approved->each(function ($item) use ($mail, $payment_id, &$domains) {
					Domain::create(array(
						'name' => $item->name,
						'tld' => $item->tld,
						'admin' => $item->user,
						'billing' => $item->billing,
						'tech' => $item->billing,
						'payment_id' => $payment_id,
						'registrant' => $item->full_name,
						'registrant_addr' => $item->addr_1,
						'isLocal' => $item->local,
						'country' => $item->country,
						'dns_hosts' => $item->dns_hosts,
						'dns_ips' => $item->dns_ips,
						'dsrecord' => $item->dsrecord,
						'expire' => Carbon::now()->addWeeks(2)
					));

					array_push($domains, $item->name . '.' . $item->tld);
					//$invoice['size']++;
					$item->delete();
				});

				//Send invoice
				$invoice['domains'] = $domains;
				Mail::queue('emails.invoice', $invoice, function ($message) use ($mail) {
					$message->to($mail, 'TTNIC Admin')->subject('TTNIC Invoice');
				});

			}
		}

		return redirect(action('AdminController@index'));
	}

	/**
	 * Returns the total cost of the domains
	 *
	 * @param $cart 	domains to find total cost of
	 * @param $local	Flag to for finding TTD or USD total
	 * @return float	Total cost
	 */
	public function total ($cart, $local){
		$total = 0.0;

		if($local) {
			$cart->each(function ($item) use (&$total) {
				if ($item->local) {
					if(strlen($item->name) > 1){
						$total = $total + $item->topLevelDomain->cost_tt;
					}
					else {
						$total = $total + $item->topLevelDomain->cost_tt_single;
					}
				}

			});
		}
		else{
			$cart->each(function ($item) use (&$total) {
				if (!$item->local) {
					if(strlen($item->name) > 1){
						$total = $total + $item->topLevelDomain->cost_us;
					}
					else {
						$total = $total + $item->topLevelDomain->cost_us_single;
					}
				}

			});
		}
		return $total;
	}

	/**
	 * Creates a Geo Chart based on all domains
	 *
	 * @return mixed	Geo Chart
	 */
	public function charts(){
		$locations = Lava::DataTable();
		$countries = Domain::distinct()->select('country')->get();
		$locations->addStringColumn('Country')
			->addNumberColumn('Domains');

		foreach($countries as $country){
			$count = Domain::where('country',$country['country'])->count();
			$name = Countries::where('iso_3166_2',$country['country'])->first();
			$entry = [$name->name, $count];
			$locations = $locations->addRow($entry);
		}
		Lava::TextStyle([
			'color' => '#F4C6B2',
			'fontName' => 'Arial',
			'fontSize' => 16
		]);
 		$bg = Lava::BackgroundColor([
			'stroke' => '#F4C6B2',
			'strokeWidth' => 0,
			'fill' => 'transparent'
		]);
		$ca = Lava::ColorAxis([
			'colors' => array('#00B5AE', '#144BC0')
		]);

		$geochart = Lava::GeoChart('Popularity')
			->setOptions(array(
				'datatable' 		=> $locations,
				//'displayMode'		=> 'markers',
				'backgroundColor'	=> $bg,
				'keepAspectRatio'	=> true,
				'markerOpacity'		=> 0.5,
				'colorAxis'		=> $ca
			));

		return $geochart;
	}

	/**
	 * Toggles the activation status for a given domain
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function toggleActivation(){
		$v = Validator::make(Request::all(), [
			'id' => 'required|numeric',
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v);;
		}

		$domain = Domain::findOrFail(Request::input('id'));

		$domain->activated = !$domain->activated;
		$domain->save();
		return redirect()->back();
	}

	/**
	 * Extends the expiration date for a given domain
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse
	 */
	public function extend(){
		$v = Validator::make(Request::all(), [
			'id'	 	=> 'required|numeric',
			'months'	=> 'required|numeric|min:0.5|max:6',
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v);;
		}

		$domain = Domain::findOrFail(Request::input('id'));

		$domain->expire = $domain->expire->addMonths(Request::input('months'));
		$domain->save();
		return redirect()->back();
	}

	public function users(){

		$users = User::where('isAdmin', false)->get();

		return view('admin.users')->with('users',$users);
	}
}

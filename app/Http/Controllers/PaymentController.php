<?php namespace App\Http\Controllers;

use App\Cart;
use App\Events\DomainWasPurchased;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PurchaseRequest;
use App\Http\Requests\PurchaseViewRequest;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\MessageBag;

/**
 * Class PaymentController
 *
 * Handles payments and stripe calls for the application
 * @package App\Http\Controllers
 */
class PaymentController extends Controller {

	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * Display a listing of payments.
	 *
	 * @return Response
	 */
	public function index()
	{
		$payments = Payment::userPayments()->unPaid()->get();
		return view('payments.index')->with('payments',$payments);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		abort(404);
		return view('payments.create');
	}

	/**
	 * Store a newly created payment.
	 *
	 * @return Response
	 */
	public function store()
	{

		$request = \Illuminate\Support\Facades\Request::all();
		$payment = Payment::findOrFail($request['id']);

		$info['receipt_email'] = Auth::user()->email;
		$info['currency'] = 'USD';
		$total_tt = 0;
		$total_us = 0;
		$paid_tt = true;
		$paid_us = true;
		$errors = new MessageBag();
		$user = Auth::user()->subscription('domain_tt')->create($request['stripeToken']);

		//Calculates total
		if($payment->amount_tt > 0){
			$total_tt = $payment->amount_tt * 100 * intval($request['years']);
		}
		if($payment->amount_us > 0){
			$total_us = $payment->amount_us * 100 * intval($request['years']);
		}
		//Perform Charge USD
		if($total_us){
			$paid_us = Auth::user()->charge($total_us, $info);
			if(!$paid_us){
				$errors->add('USD','Payment of $'.$total_us.'USD was unsuccessful');
			}
			$payment->amount_us = -$payment->amount_us;

		}
		//Perform Charge USD
		if($total_tt){
			$info['currency'] = 'TTD';
			$paid_tt = Auth::user()->charge($total_tt, $info);
			if(!$paid_us){
				$errors->add('TTD','Payment of $'.$total_tt.'TTD was unsuccessful');
			}
			$payment->amount_tt = -$payment->amount_tt;
		}

		//Checks if payment was sucessful
		if($paid_tt && $paid_us){
			$payment->paid = true;
			foreach($payment->domains as $domain){
				$domain->activated=true;
				$domain->expire = $domain->expire->addYears(intval($request['years']));
				$domain->save();
			}
			$payment->save();
			return redirect(action('PaymentController@index'))->with('message',"Payment Successful");
		}
		$payment->save();
		return redirect(action('PaymentController@index'))->withErrors($errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		abort(404);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		abort(404);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		abort(404);
	}

	public function total_tt ($cart){
		$total = 0.0;

		$cart->each(function($item) use (&$total){
			$total = $total + $item->topLevelDomain->cost_tt;

		});

		return $total;
	}

	public function total_us ($cart){
		$total = 0.0;

		$cart->each(function($item) use (&$total){
			$total = $total + $item->topLevelDomain->cost_us;

		});

		return $total;
	}

	public function stripe(){

		$cart = Cart::items()->submitted()->payable()->get();
		$js_config = array(
			'total_tt' => $this->total_tt($cart),
			'total_us' => $this->total_us($cart),
			'desc' => "Domain Payments",
		);
		return view('payments.stripeemb')->with('config', $js_config);
	}
}

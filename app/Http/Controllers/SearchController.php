<?php namespace App\Http\Controllers;

use App\Cart;
use App\Domain;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tld;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use True\Punycode;

class SearchController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function search()
	{

//		$v = Validator::make(Request::all(), [
//			'term' 	=> 	array('required','min:1','regex:/(\w+(-\w+)+)|(\w+)/'),
//		]);
		$v = Validator::make(Request::all(), [
			'term' 	=> 	array('required','min:1'),
		]);
		$puny = new Punycode();
		$encode = $puny->encode(Request::input('term'));
		if(starts_with($encode, 'xn--'))
			$encode = substr($encode, 4);
		$m = array();
		preg_match('/(\w+(-\w+)+)|(\w+)/',$encode, $m);

//		$m = array();
//		preg_match('/(\w+(-\w+)+)|(\w+)/',Request::input('term'), $m);

		if ($m[0] != $encode || $v->fails())
		{
			return redirect(action('HomeController@index'));
		}
		$term = Request::input('term');
		$results = Domain::where('name',$term)->get();
		$tlds = Tld::all();

		$cart = null;
		if(Auth::check()){
			$cart = Cart::items()->where('name',$term)->get();
		}

		return view('search.results')->with('results', $results)->with('tlds',$tlds)->with('term',$term)->with('cart', $cart);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param $term
	 * @return Response
	 * @internal param int $id
	 */
	public function show($term)
	{
//		$v = Validator::make(['term' => $term], [
//			'term' 	=> 	array('required','min:1','regex:/(\w+(-\w+)+)|(\w+)/'),
//		]);
		$v = Validator::make(['term' => $term], [
			'term' 	=> 	array('required','min:1'),
		]);
		$puny = new Punycode();
		$encode = $puny->encode($term);
		if(starts_with($encode, 'xn--'))
			$encode = substr($encode, 4);

		$m = array();
		preg_match('/(\w+(-\w+)+)|(\w+)/',$encode, $m);

		if ($m[0] != $encode || $v->fails())
		{
			return redirect(action('HomeController@index'));
		}
		$cart = null;
		if(Auth::check()){
			$cart = Cart::items()->where('name',$term)->get();
		}
		$results = Domain::where('name', '=', $term)->get();
		$tlds = Tld::all();
		return view('search.results')->with('results', $results)->with('tlds',$tlds)->with('term',$term)->with('cart', $cart);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

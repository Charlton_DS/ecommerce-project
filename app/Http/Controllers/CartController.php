<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Cart;
use App\Http\Controllers\Controller;

use App\Http\Requests\CartSubmitRequest;
use App\Http\Requests\DeleteCartItemRequest;
use App\Http\Requests\RegisterDomainRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Webpatser\Countries\Countries;

/**
 * Class CartController
 *
 * This Controller handles manipulation of cart items for a user
 * @package App\Http\Controllers
 */
class CartController extends Controller {


	public function __construct(){
		$this->middleware('auth');
	}
	/**
	 * Display a listing of cart items.
	 *
	 * @return Response
	 */
	public function index()
	{
		$items = Cart::items()->pending()->get();
		$counties = Countries::all();
		//return $counties;
		return view('cart.index')->with('items',$items)->with('countries',$counties);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		abort(404);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RegisterDomainRequest $request)
	{
		Auth::user()->email;
		$item = new Cart;

		$item->name = $request->name;
		$item->tld = $request->tld;
		$item->user = Auth::user()->email;
		$item->billing = $item->user;
		$item->technical = $item->user;
		$item->approved = null;
		$item->submitted = false;
		$item->save();

		$items = Cart::where('user', Auth::user()->email)->pending()->get();

		return redirect()->action('SearchController@show', [$item->name])->with('message','Added to cart');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(RegisterDomainRequest $request, $name, $tld)
	{

		Auth::user()->email;
		$item = new Cart;

		$item->name = $request->name;
		$item->tld = $request->tld;
		$item->user = Auth::user()->email;
		$item->approved = null;
		$item->submitted = false;
		$item->save();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		abort(404);
	}

	/**
	 * Deletes a specified item from the cart.
	 *
	 * @param DeleteCartItemRequest $request
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy(DeleteCartItemRequest $request)
	{

		$item = Cart::findOrFail($request->id);
		Cart::destroy($request->id);
		return redirect()->action('SearchController@show', [$item->name])->with('message','Deleted from Cart');

	}

	/**
	 * Deletes a specified item from the cart
	 *
	 * @param DeleteCartItemRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete(DeleteCartItemRequest $request)
	{

		$item = Cart::findOrFail($request->id);
		Cart::destroy($request->id);
		return redirect()->action('SearchController@show', [$item->name])->with('message','Deleted from Cart');

	}

	/**
	 * Changes items in cart to submitted
	 *
	 * @param CartSubmitRequest $request
	 * @return $this
	 */
	public function submit(CartSubmitRequest $request)
	{
		$domains = \Illuminate\Support\Facades\Request::input('domain');
		$items = Cart::items()->pending()->get();
		$input['size'] = count($items);
		$input['name'] = Auth::user()->name;
		$input['email'] = Auth::user()->email;
		$items->each(function($item) use (&$domains){
			if(isset($domains[$item->id])) {
				$item->full_name = $domains[$item->id]['name'];
				$item->addr_1 = $domains[$item->id]['address'];
				$item->dns_hosts = $domains[$item->id]['hosts'];
				$item->dns_ips = $domains[$item->id]['ips'];
				if(isset($domains[$item->id]['technical'])) {
					$t = User::where('email', $domains[$item->id]['technical'])->first();
					if($t != null){
						$item->technical = $domains[$item->id]['technical'];
					}
				}
				if(isset($domains[$item->id]['billing'])) {
					$b = User::where('email', $domains[$item->id]['billing'])->first();
					if($b != null){
						$item->billing = $domains[$item->id]['billing'];
					}
				}

				$item->phone = "00000";
				$item->country = $domains[$item->id]['country'];
				$item->submitted = true;
				$item->save();
			}
		});
		Mail::queue('emails.request', $input, function($message)
		{
			$message->to(env('ADMIN_EMAIL'), 'TTNIC Admin')->subject('New Domain Approval');
		});

		return view('cart.index')->with('message','Domains have been submitted for approval.');
	}

	public function add(RegisterDomainRequest $request)
	{
		abort(404);
//		Auth::user()->email;
//		$item = new Cart;
//
//		$item->name = $request->name;
//		$item->tld = $request->tld;
//		$item->user = Auth::user()->email;
//		$item->approved = null;
//		$item->submitted = false;
//		$item->save();
//
////		Mail::send('emails.request', $item->ToArray(), function($message)
////		{
////			$message->to(env('ADMIN_EMAIL'), 'TTNIC Admin')->subject('New Domain Approval');
////		});
//		$items = Cart::where('user', Auth::user()->email)->pending()->get();
//		//return $items;
//		return view('domains.create')->with('cart',$items);
	}

}

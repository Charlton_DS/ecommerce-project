<?php namespace App\Http\Requests;

use App\Cart;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class DeleteCartItemRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		$id = Request::input('id');

		return Cart::items()->where('id',$id)->first();
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'id' => 'required'
		];
	}

}

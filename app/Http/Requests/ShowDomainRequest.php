<?php namespace App\Http\Requests;

use App\Domain;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class ShowDomainRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		//$id = $this->route('domain');
		//return Auth::user()->adminDomains()->find($id) || Auth::user()->technicalDomains()->find($id);
		//return Domain::where('id', $id)->where('admin', Auth::user()->email)->exist() || Domain::where('id', $id)->where('tech', Auth::user()->email)->exist();
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'id' => 'required|numeric'
		];
	}

}

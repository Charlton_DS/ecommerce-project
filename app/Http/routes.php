<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use App\Cart;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

Route::get('/', 'WelcomeController@index');

Route::get('faq', function()
{
	return view('faq');
});

Route::get('policy', function()
{
	return view('policy');
});

Route::get('fees', function()
{
	return view('fees');
});

Route::get('termsAndConditions', function()
{
	return view('termsAndConditions');
});

Route::get('contactUs', function()
{
	return view('contactUs');
});

Route::get('aboutUs', function()
{
	return view('aboutUs');
});

Route::get('home', 'HomeController@index');

Route::group(['prefix' => 'admin'], function()
{
	Route::get('home','AdminController@index');
	Route::post('approve', 'AdminController@approve');

	Route::post('toggle', 'AdminController@toggleActivation');
	Route::post('extend', 'AdminController@extend');

	Route::get('users', 'AdminController@users');
	Route::get('approve', function()
	{
		$users = Cart::distinct()->select('user')->submitted()->notApproved()->orderBy('user')->get();
		$domains = Cart::submitted()->notApproved()->orderBy('user')->get();
		return view('admin.approval')->with('domains',$domains)->with('users',$users);
	});
});

Route::post('search','SearchController@search');

Route::post('delete','CartController@delete');

Route::get('search/{term}','SearchController@show');

Route::get('addToCart','CartController@add');

Route::resource('domain','DomainsController');

Route::resource('profile','ProfileController');

Route::resource('payment','PaymentController');

Route::post('cart/submit','CartController@submit');

Route::get('sfa','SecondFactorController@enable');

Route::post('sfa/verify','SecondFactorController@verify');

Route::resource('cart','CartController');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Payment extends Model {

	//

    protected $table = 'payments';

    protected $fillable = ['type', 'amount_us', 'amount_tt', 'user_id'];


    public function owner(){

        return $this->belongsTo('App\User');
    }

    public function domains(){

        return $this->hasMany('App\Domain');
    }

    public function scopeUserPayments($query){
        $query->where('user_id',Auth::user()->id);
    }
    public function scopeUnPaid($query){
        $query->where('paid',false);
    }


}

<?php namespace App;
class Zone{
    var $domainName;
    var $rootName;
    var $zoneFile;

    private static $zone = NULL;


    private function __construct(){
    }

    static  function getZone(){
        if(self::$zone == NULL)
            self::$zone = new Zone();

        return self::$zone;
    }
    function setDomainName($a){
        $this->domainName = $a;
    }
    function setHostMasterName($b){
        $this->rootName = $b;
    }

    function createFile(){
        $this->zoneFile = fopen($this->domainName.".zone","w");
    }

    function addOrigin(){

        fwrite($this->zoneFile,'$ORIGIN  '.$this->domainName."\n");
    }

    function addSOA($SOArefresh,$SOAexpire,$SOAretry,$SOATTL,$SOAserial){
        file_put_contents($this->domainName.".zone",
            "@\tIN\tSOA\t".$this->domainName."\t".$this->rootName."(\t
   		".$SOAserial."\t".$SOArefresh."\t".$SOAretry."\t".$SOAexpire."\t".$SOATTL.")\n
   		",
            FILE_APPEND);
    }

    function inputArecord($host,$ipaddress){
        file_put_contents($this->domainName.".zone",$host."\tIN\tA\t".$ipaddress."\n",FILE_APPEND);
    }

    function nameServerRecord($nameServer){
        file_put_contents($this->domainName.".zone","IN\tNS\t".$nameServer."\n",FILE_APPEND);
    }

    function addDNSSEC($mode,$type){
        $shash=hash('md5',date("Ymd"));
        file_put_contents($this->domainName.".zone",$this->domainName."\t10\tIN\t".$mode."\t".$type."\t5\t2\t100\t".date('Ymd') ."(".$shash.")\n",FILE_APPEND);
    }
    function addDS($nameserver,$keyTag,$methodType){
        $shash=hash('md5',date("Ymd"));
        file_put_contents($this->domainName.".zone",$nameserver."\tIN\tDS\t".$keyTag."\t".$methodType."\t1".date('Ymd')."(".$shash.")\n",FILE_APPEND);
    }

    function setTTL($days){
        file_put_contents($this->domainName.".zone",'$TTL  '.$days.'D'."\n",FILE_APPEND);
    }

    function getDomain(){
        echo $this->domainName;
    }
}//END OF CLASS


//STEPS /////////////////////////////////////////////////////////
/*
    1.Create the only instance of the zone calling the getZone()

    2. Give the zone a primary name server by the setDomainName()

    3. The email of the person to contact about the primary name server is set by HostMasterName()

    4. Create a blank file with createFile()

    5. First set the orgin domain with the addOrigin()

    6. Set the default time to live value with setTTL(days)

    7. Add the authorative record addSOA(time to refresh, time to expire, time to retry, minimum time to live, serial number)
        NOTE: time can be in seconds, days and weeks e.g 86400, 7D and 1W and for serial number its good to use the current date eg 18042015

    8. From here you can append nameservers or either A records with nameServerRecord(nameserver) or inputArecord()

    9. If the user requests DNSSEC, then after the name server record use addDS(nameserver,keytag,method type, digest Type)
       NOTE: the digest type =1 , goes is only one type we are using
*/

?>
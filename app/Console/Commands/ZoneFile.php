<?php namespace App\Console\Commands;

use App\Domain;
use App\Zone;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class ZoneFile extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'zone';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates a new Zone file.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Creating new Zone File');
		$zone = Zone::getZone();

		$domains = Domain::activate()->get();

		$zone->setDomainName("tcw.tt");
		$zone->setHostMasterName('hostmaster.tcw.tt');
		//$zone->setRootName("root.tcw.tt");
		$zone->createFile();
		$zone->addOrigin();
		$zone->setTTL(3);
		$zone->addSOA('1H','1W','15M','1D',date("Ymd"));
		//$zone->addDNSSEC('RRSIG','A');
		foreach($domains as $domain) {

			$hosts = explode(PHP_EOL,$domain->dns_hosts);
			$ips = explode(PHP_EOL,$domain->dns_ips);
			if(count($hosts) > 0 && count($ips) > 0){
				$zone->nameServerRecord($domain->name.'.'.$domain->tld);
				for ($i=0; $i < count($hosts); $i++) {
					if(isset($hosts[$i]) && isset($ips[$i])) {
						$zone->inputArecord($hosts[$i], $ips[$i]);
					}
				}
			}
			$zone->addDS($domain->name.'.'.$domain->tld,6443,7);
		}


		$this->info('Finished Creating new Zone File');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}

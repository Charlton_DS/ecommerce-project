<?php namespace App\Console\Commands;

use App\Domain;
use App\Payment;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MaintainDomains extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'expiration';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends maintenance emails';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Checking for expiring domains');
		$billingEmails = Domain::distinct()->select('billing')->expiresSoon()->orderBy('billing')->get();
		$this->info('Found '.count($billingEmails).' users with expiring domains');
		foreach($billingEmails as $email){
			$domains = Domain::where('billing',$email->billing)->expiresSoon()->get();

			if(count($domains) > 0) {
				$this->info('Found '.count($domains).' expiring domains for '.$email->billing);
				$invoice['size'] = count($domains);
				$payment = Payment::create(array(
					'type' => "Domain Maintenance",
					'amount_tt' => $this->total($domains, true),
					'amount_us' => $this->total($domains, false),
					'user_id' => $domains->first()->bill->id
				));

				$payment_id = $payment->id;
				$payment->invoice = 'TT' . Carbon::now()->year . str_pad($payment_id, 5, "0", STR_PAD_LEFT);
				$payment->paid = false;
				$payment->save();

				$invoice['number'] = $payment->invoice;
				$invoice['amount_tt'] = $payment->amount_tt;
				$invoice['amount_us'] = $payment->amount_us;
				$invoice['name'] = $domains->first()->bill->name;

				$mail = $email->billing;
				$invoice['domains'] = $domains->toArray();
				Mail::queue('emails.invoice', $invoice, function ($message) use ($mail) {
					$message->to($mail, 'TTNIC Admin')->subject('TTNIC Invoice');
				});
			}

		}
		$this->info('Done');
	}

	public function total ($domain, $local){
		$total = 0.0;

		if($local) {
			$domain->each(function ($item) use (&$total) {
				if ($item->isLocal) {
					if(strlen($item->name) > 1){
						$total = $total + $item->topLevelDomain->cost_tt_single;
					}
					else {
						$total = $total + $item->topLevelDomain->cost_tt;
					}
				}

			});
		}
		else{
			$domain->each(function ($item) use (&$total) {
				if (!$item->isLocal) {
					if(strlen($item->name) > 1){
						$total = $total + $item->topLevelDomain->cost_us_single;
					}
					else {
						$total = $total + $item->topLevelDomain->cost_us;
					}
				}

			});
		}
		return $total;
	}


//	/**
//	 * Get the console command arguments.
//	 *
//	 * @return array
//	 */
//	protected function getArguments()
//	{
//		return [
//			['example', InputArgument::REQUIRED, 'An example argument.'],
//		];
//	}
//
//	/**
//	 * Get the console command options.
//	 *
//	 * @return array
//	 */
//	protected function getOptions()
//	{
//		return [
//			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
//		];
//	}

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tld extends Model {

	//
    protected $table='tlds';

    protected $fillable = [
        'tld',
        'cost_us',
        'cost_tt'
    ];

    public function carts(){

        return $this->hasMany('App\Cart','tld','tld');
    }
}

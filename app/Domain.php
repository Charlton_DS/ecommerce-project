<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model {

	//
    protected $table='domains';

    protected $dates=['expire'];

    protected $fillable = [
        'name',
        'tld',
        'admin',
        'billing',
        'tech',
        'payment_id',
        'registrant',
        'registrant_addr',
        'country',
        'expire',
        'dns_host',
        'dns_ips',
        'isLocal'
    ];



    /**
     * A domain has one owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function owner(){

        return $this->belongsTo('App\User','admin','email');
    }

    public function bill(){

        return $this->belongsTo('App\User','billing','email');
    }

    public function technical(){

        return $this->belongsTo('App\User','tech','email');
    }

    public function payment(){

        return $this->belongsTo('App\Payment');
    }

    public function topLevelDomain(){

        return $this->belongsTo('App\Tld', 'tld', 'tld');
    }

    public function scopeActivate($query){
        $query->where('activated',true);
    }

    public function scopeExpiresSoon($query){
        $query->where('expire', Carbon::today()->addMonths(1));
    }


    public function setExpireAttribute($date){
        $this->attributes['expire'] = Carbon::parse($date);
    }
}

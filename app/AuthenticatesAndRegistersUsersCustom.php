<?php namespace App;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Jenssegers\Agent\Facades\Agent;
use PragmaRX\Google2FA\Google2FA;

trait AuthenticatesAndRegistersUsersCustom {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The registrar implementation.
     *
     * @var Registrar
     */
    protected $registrar;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->auth->login($this->registrar->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::validate($credentials))
        {
            $user = User::where('email', '=', $request->email)->get()->first();

            //return response()->view('auth.sfa')->withCookie(cookie('path',$this->redirectPath(),5))->withCookie(cookie('email',$request->email,5));
            if(isset($user->secret2fa)){
                //return $this->gentoken($user->email).'-------'.$request->cookie('token2fa');
                //return var_dump($this->gentoken($user->email) == $request->cookie('token2fa'));
                if ($this->gentoken($user->email) == $request->cookie('token2fa') ){
                    $this->auth->attempt($credentials, $request->has('remember'));
                    return redirect()->intended($this->redirectPath());
                }
                return response()->view('auth.sfa')->withCookie(cookie('path',$this->redirectPath(),2))->withCookie(cookie('email',$user->email,2))->withCookie(cookie('remember',$request->has('remember'),2));
            }
        }

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath())
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'These credentials do not match our records.',
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        $this->auth->logout();

        return redirect('/');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/auth/login';
    }

    public function postSfa(Request $request){
        $email = $request->cookie('email');
        $path = $request->cookie('path');





        $user = User::where('email', '=', $email)->get()->first();
        $google2fa = new Google2FA();

        if($google2fa->verifyKey($user->secret2fa,$request->secret)){
            if($request->remember2fa){
                Auth::login($user);
                return response()->redirectTo($path)
                    ->withCookie(cookie()->forever('token2fa',$this->gentoken($user->email)))
                    ->withCookie(Cookie::forget('email'))
                    ->withCookie(Cookie::forget('path'))
                    ->withCookie(Cookie::forget('remember'));
            }
            Auth::login($user);
            return response()-> redirectTo($path)
                ->withCookie(Cookie::forget('email'))
                ->withCookie(Cookie::forget('path'))
                ->withCookie(Cookie::forget('remember'));
        }
        return view('auth.sfa')
            ->withErrors([
                'code' => 'This code is wrong.',
            ]);
    }

    public function gentoken($input){
        return Agent::device().Agent::platform().Agent::browser().Agent::version(Agent::platform()).$input;
    }

    public function comptoken($input,$ctext){
        return $this->gentoken($input) == $ctext;
    }
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model {

	//
    protected $table = 'carts';



    public function customer(){

        return $this->belongsTo('App\User','user','email');
    }

    public function customerBilling(){

        return $this->belongsTo('App\User','billing','email');
    }

    public function customerTechnical(){

        return $this->belongsTo('App\User','technical','email');
    }

    public function topLevelDomain(){

        return $this->belongsTo('App\Tld','tld','tld');
    }

    public function scopeItems($query){
        $query->where('user',Auth::user()->email);
    }

    public function scopePending($query){
        $query->where('submitted',false);
    }

    public function scopeSubmitted($query){
        $query->where('submitted',true);
    }
    public function scopePayable($query){
        $query->where('approved',true);
    }
    public function scopeDenied($query){
        $query->where('approved',false);
    }

    public function scopeNotApproved($query){
        $query->where('approved',null);
    }

}

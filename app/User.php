<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract , BillableContract{

	use Authenticatable, CanResetPassword, Billable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','isAdmin'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * The dates needed for stripe
	 * 
	 * @var array
	 */
	protected $dates = ['trial_ends_at', 'subscription_ends_at'];


	/**
	 * A user can own many domains
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function adminDomains(){

		return $this->hasMany('App\Domain','admin','email');
	}

	public function billingDomains(){

		return $this->hasMany('App\Domain','billing','email');
	}

	public function technicalDomains(){

		return $this->hasMany('App\Domain','tech','email');
	}

	public function cart(){

		return $this->hasMany('App\Cart','user','email');
	}

	public function payments(){

		return $this->hasMany('App\Payment');
	}
}

#!/bin/sh

# Clone project from bitbucket
echo "Checking for ecommerce-project"
if [ -d ecommerce-project ]
	then
		echo "project found"		
		cd ecommerce-project
		echo "doing pull from repo..."
		git pull
		cd ..
		echo "done!"
	else
		echo "Not Found, cloning...."
		git clone https://Charlton_DS@bitbucket.org/Charlton_DS/ecommerce-project.git
fi

echo "copying /public to /www"
cp -r ecommerce-project/public/* www/
echo "done!"

echo "copying application files"
cp -r ecommerce-project/ .
echo "done!"



@extends('app')

@section('content')

    <div class="ui three column centered grid">

        <div class="column"></div>
        <div class="column">

            {!! HTML::image($url, null ,['class' => 'ui centered medium bordered image']) !!}
            @if (count($errors) > 0)
                <div class="ui negative message"><!-- put in red-->
                    <strong>Whoops!</strong> Your token did not match, scan the code and try again.
                </div>
            @endif
            {!! Form::open(['action' => 'SecondFactorController@verify', 'class' => 'ui form']) !!}

            <div class="field">
                <label>Secret</label>
                {!! Form::text('secret','',['placeholder' => '6 - Digit Code', 'required'])!!}
            </div>
            <div class="ui hidden divider"></div>
            <div class="field">
                <button class="ui positive button" type="submit">
                    Verify
                </button>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="column"></div>
    </div>
@stop
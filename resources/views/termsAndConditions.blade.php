@extends('app')

@section('content')
<br/>
	<h3 class="ui red centered header">TERMS AND CONDITIONS FOR THE REGISTRATION OF A TT INTERNET DOMAIN NAME</h3>
	
	<div class="ui compact green segment">
		<div class="ui ordered divided list">
			<div class="item"><h4>Definitions</h4>
				<div class="list">
					<p class="item"><b>Administrative Contact</b> means the person or organization responsible for a Domain Name and to whom all enquiries relating to the Domain Name may be sent.</p>
					<p class="item"><b>Billing Contact</b> means the person or organization responsible for making any payments due under this Registration Agreement to the Registrar.</p>
					<p class="item"><b>Domain Name</b> means the domain name assigned by the Registrar to the Registrant.</p>
					<p class="item"><b>Domain Name Application</b> means the form published by the Registrar for the purposes of soliciting information from persons or entities seeking to register an Internet domain name, which can be found on the World Wide Web at http://www.nic.tt/cgi-bin/fsub.pl.</p>
					<p class="item"><b>Domain Rules</b> means the Registrar's Rules for the Domain and Sub-Domain and published on the World Wide Web at http://www.nic.tt/rules.html.</p>
					<p class="item"><b>Dispute Policy</b> means the Dispute Resolution Policy for Domain Names registered with the Registrar, and published on the World Wide Web at http://www.icann.org/en/dndr/udrp/uniform-rules.htm.</p>
					<p class="item"><b>Registration Agreement</b> shall mean the:</p>
					<p>(i) Domain Name Registration Application Form;</p>
					<p>(ii) Terms and Conditions for the Registration of a TT Internet Domain Name;</p>
					<p>(iii) Registrar's Rules for the Domain and Sub-Domain; and</p>
					<p>(iv) Dispute Resolution Policy for Domain Names registered with the Registrar.</p>
					<p class="item"><b>Registrant</b> means (i) the person(s) seeking the registration of a domain name; (ii) the registered keeper to whom a domain name has been allocated and entered into the Registry; or (iii) the registered keeper of the e-mail address of the Administrative Contact.</p>
					<p class="item"><b>Registrar</b> means the Trinidad and Tobago Network Information Centre Limited (henceforth referred to as the TTNIC) that is incorporated in the Republic of Trinidad and Tobago.</p>
					<p class="item"> <b>Registration</b> means all information supplied by the Registrant and contained within a Registrar data record.</p>
					<p class="item"><b>Registration Fee</b> shall mean the fees specified in the schedule annexed hereto or as specified by the Registrant's agent (where applicable) to be paid in connection with an application to register a domain name.</p>
					<p class="item"><b>Registry</b> means a database containing the zone file(s) of one or more Internet domain names, including lower-level domain names, corresponding Internet Protocol addresses and related data.</p>
					<p class="item"><b>Registry Administrator</b> means the person or entity responsible for administering a Registry</p>
					<p class="item"><b>Renewal Fee</b> means the fee specified in the schedule annexed hereto or as specified by the Registrant's agent (where applicable) to be paid in connection with the renewal of a domain name registration.</p>
					<p class="item"><b>Terms and Conditions</b> means the Registrar's terms and conditions governing the registration and use of a domain name registered with the Registrar and published on the World Wide Web at http://www.nic.tt/agreement.html.</p>
					<p class="item"><b>Tradename</b> means a name used in the course of trade and by way of business.</p>
					<p class="item"><b>Transferee</b> means a person or body to whom the Domain Name is assigned.</p>
				</div>
			</div>
			<div class="item"><h4>Scope</h4>
				<div class="list">
					<p class="item">These Terms and Conditions shall apply to any application to register an Internet domain name with the TTNIC and the subsequent registration and use, direct or indirect, of the Domain Name.</p>
					<p class="item">By completing and submitting a Domain Name Application for consideration and acceptance by the Registrar, the Registrant acknowledges that he has read the Terms and Conditions.</p>
					<p class="item">In the event that a domain name is registered in the Registrar's domain name database and assigned to the Registrant, the Registrant agrees to be bound by the Terms and Conditions.</p>
				</div>
			</div>
			<div class="item"><h4>Fees and Payments</h4>
				<div class="list">
					<p class="item"> As consideration for the registration of the Domain Name with the Registrar, the Registrant agrees to pay the Registration Fee and, when applicable, the Renewal Fee.</p>
					<p class="item">Receipt of the Registration Fee by the Registrar, or the Registrar's duly appointed agent, shall constitute confirmation of the Registrant's acceptance of these Terms and Conditions.</p>
					<p class="item">The Registrar shall have no obligation to activate a Domain Name unless and until it has received payment of the Registration Fee or Renewal Fee, as the case may be.</p>
					<p class="item">Except as specified herein, all payments made in connection with the registration of the Domain Name, or the renewal of the Domain Name registration, shall be non-refundable.</p>
					<p class="item">The Registration Fee shall cover a period of three (3) years for each new registration, and three (3) years for each renewal, and includes any permitted modification(s) to the Domain Name's record during the covered period. At the conclusion of such registration period(s), as the case may be, failure by or on behalf of the Registrant to pay the Renewal Fee within the time specified in a second notice or reminder shall result in cancellation of the Domain Name registration.</p>
					<p class="item">In the event that the Registrant uses an agent for the purposes of making any required payments, the Registrar shall first seek payment from the agent. In the event that the agent fails to make such payment, for whatever reason, the Registrar shall have the right to recover the payment from the Registrant.</p>
					<p class="item">Payment of the Registration Fee or the Renewal Fee, as the case may be, shall be made to the Registrar within thirty (30) days of the date appearing on the invoice.</p>
					<p class="item">he Registration Fee and the Renewal Fee, as the case may be, shall be paid by such means and methods as may be specified from time to time by the Registrar.</p>
					<p class="item">Acceptable forms of payment are Credit Card, Cheque, Cash and Wire Transfer.</p>
				</div>
			</div>
			<div class="item"><h4>Accuracy of Information</h4>
				<div class="list">
					<p class="item">For the purposes of applying to register the Domain Name, the Registrant shall be required to provide the information requested in the Domain Name Application. The Registrar shall take reasonable precautions to protect all such information from loss, misuse, unauthorized access or disclosure, alteration or destruction.</p>
					<p class="item">By applying to register a Domain Name, the Registrant represents and warrants that, to the best of the Registrant's knowledge and belief, any information provided in the Domain Name Application is true and correct.</p>
					<p class="item">The Registrant represents and warrants that any future changes to the information provided in the Domain Name Application required to maintain its accuracy shall be provided to the Registrar in an expeditious manner in accordance with the domain name modification procedures in place at that time.</p>
					<p class="item">The Registrar shall have no obligation to screen information submitted by the Registrant to determine the accuracy of the information held by the Registrar nor to determine whether the information may infringe upon the right(s) of a third party, but may rely on the representations and warranties provided by the Registrant. The Registrant agrees that, by the registration of the Domain Name, such registration does not confer immunity from objection to either the registration or use of the Domain Name by any party.</p>
					<p class="item">The Registrar reserves the right to cancel the Domain Name if this Registration Agreement, or subsequent modification(s) thereto, contains false or misleading information, or conceals or omits any information the Registrar would likely have considered material to its decision to accept the Domain Name Application and assign the Domain Name to the Registrant.</p>
					<p class="item">A Registrant's willful provision of inaccurate or unreliable information, its willful failure promptly to update information provided to the Registrar, or its failure to respond for over fifteen (15) calendar days to inquiries by the Registrar concerning the accuracy of contact details associated with the Domain Name shall constitute a material breach of the Registration Agreement and shall be a basis for cancellation of the registration of the Domain Name.</p>
					<p class="item">The Registrar's remedies against the Registrant for any breach of this clause shall continue to be available notwithstanding any modification, surrender, cancellation or transfer of the registration of the Domain Name.</p>
					<p class="item">The Registrant represents and warrants that it has exercised its best efforts to determine that neither the registration of the Domain Name nor the manner in which it will be directly or indirectly used shall infringe upon or otherwise violate the legal rights of any third party.</p>
					<p class="item">The Registrant represents and warrants that, to the best of the Registrant's knowledge, the registration and use, direct and indirect, of the Domain Name does not violate any applicable laws or regulations.</p>
					<p class="item">In the event the Registrant licenses the use of the Domain Name to a third party, the Registrant shall nonetheless continue to be considered the holder of record of the Domain Name and shall be responsible for (i) providing its own full contact information, (ii) providing full contact information for its licensee, and (iii) and for providing and updating accurate technical, administrative, and other contact information adequate to facilitate timely resolution of any problems that may arise in connection with the Domain Name.</p>
					<p class="item">The Registrar shall, upon notification by any person of an inaccuracy in the information provided in the Domain Name Application, take reasonable steps to investigate that claimed inaccuracy. In the event the Registrar ascertains that inaccurate information has been provided, it shall take reasonable steps to correct any such inaccuracy.</p>
				</div>
			</div>
			<div class="item"><h4>Acceptance</h4>
				<p>The Registration Agreement shall be deemed accepted at the offices of the Registrar following the receipt of all payments by the Registrar in accordance with Article 3.</p>
			</div>
			<div class="item"><h4> Confidentiality</h4>
				<div class="list">
					<p class="item">By submitting the Domain Name Application and in the event that the Domain Name is registered, the Registrant, the Registrant's agent (where applicable) and the Billing Contact hereby give their consent to the inclusion of their names, contact details and other details relating to the registration of the Domain Name in the Registry.</p>
					<p class="item">The Registrar shall be permitted by the Registrant and the Billing Contact and or the Registrant's agent to allow other organizations and members of the public to access any information provided by the Registrant in connection with the Domain Name for the purpose of obtaining information about the registration of the Domain Name or any other related purpose.</p>
					<p class="item"> The Registrant or the Registrant's agent, as the case may be, shall expressly obtain the consent of third party individuals whose personal data is to be held in the Registry and if such consent is withheld or withdrawn then the agent shall immediately terminate the registration.</p>
				</div>
			</div>
			<div class="item"><h4> Domain name rules</h4>
				<p>The Registrar will process the application and consider whether to accept or reject it in accordance with the terms of the Registration Agreement, which shall include the criteria laid down in the Registrar's Domain Rules in force at the time of the application for registration, transfer or renewal.</p>
			</div>
			<div class="item"><h4>First Come, First Served</h4>
				<p>The Registrar shall register domain names on a first come, first served basis. The Registrant agrees not to take any steps in reliance upon the prospective registration of a domain name before it becomes a registration entered in the Register of Domain Names.</p>
			</div>
			<div class="item"><h4>Right of Refusal</h4>
				<div class="list">
					<p class="item">The Registrar, in its sole and absolute discretion, reserves the right to refuse to register a domain name.</p>
					<p class="item">The Registrant agrees that the submission of an application to register a domain name does not in any way obligate the Registrar.</p>
					<p class="item"> The Registrant agrees that the Registrar shall not be liable for loss or damages that may result from the Registrar's refusal to accept an application by the Registrant to register a domain name.</p>
					<p class="item"> In the event an application is not accepted, the Registrar undertakes to notify promptly the Registrant or the Registrant's agent and return any payments received.</p>
				</div>
			</div>
			<div class="item"><h4>Dispute Policy</h4>
				<p>In the event that the Domain Name is registered, the Registrant agrees to be bound by the Dispute Policy that is incorporated herein by reference and made a part hereof. The version of the Dispute Policy currently in effect can be found at: http://www.icann.org/en/dndr/udrp/uniform-rules.htm.</p>
			</div>
			<div class="item"><h4>Third-Party Complaints</h4>
				<p>The Registrant agrees that any dispute arising out of or in connection with the use of the Domain Name or any other listing information shall be subject to the provisions specified in the Dispute Policy.</p>
				<p>or</p><br/><p>The Registrant agrees that, if any third party challenges the registration of the Domain Name, the Registrant shall be subject to the provisions specified in the Dispute Policy.</p>
			</div>
			<div class="item"><h4>Agents</h4>
				<div classs="list">
					<p class="item">Registrant agrees that if an agent for the Registrant, such as an ISP or Administrative Contact/Agent, completes this Registration Agreement the Registrant is nonetheless bound as a principal by all terms and conditions herein, including the Dispute Resolution Policy.</p>
					<p class="item">The acceptance of these terms and conditions by any agent for the Registrant (whether by e-mail or other means) shall bind such agent as if he were a principal to the Registration Agreement.</p>
				</div>
			</div>
			<div class="item"><h4>Breach</h4>
				<p>The Registrant agrees that failure to abide by any provision of this Registration Agreement or the Dispute Policy may be considered by the Registrar to be a material breach and that the Registrar may provide a written notice, describing the breach, to the Registrant. If, within thirty (30) days of the date of mailing such notice, the Registrant fails to provide evidence, which is reasonably satisfactory to the Registrar that it has not breached its obligations, then the Registrar may delete Registrant's registration of the Domain Name. Any such breach by a Registrant shall not be deemed to be excused simply because the Registrar did not act earlier in response to that, or any other, breach by the Registrant.</p>
			</div>
			<div class="item"><h4>Cancellations, Transfers, and Changes.</h4>
				<div class="list">
					<p class="item">The Registrar reserves the right to cancel, transfer or otherwise make changes to domain name registrations under the following circumstances:</p>
						<p>(i) The Registrar's receipt of written or appropriate electronic instructions from the Registrant or the Registrant's agent to </p>
						<p>(ii) The Registrar's receipt of an order from a court or arbitral tribunal, in each case of competent jurisdiction, requiring such action;</p>
						<p>(iii) The Registrar's receipt of a decision of an Administrative Panel requiring such action in any administrative proceeding to which the Registrant was a party pursuant to the Dispute Policy; and/or</p>
						<p>(iv) To comply with the resolution of a dispute concerning a domain name registered by the Registrant under another registrar's domain name dispute resolution policy that is identical to the Dispute Policy in all material respects.</p>
					<p class="item"> In the event of a material breach by the Registrant of any provision of the Registration Agreement, the Registrar, in its sole and absolute discretion, shall have the right to cancel the Domain Name registration, without any refund entitlements for the Registrant and without prejudice to any other remedies to which the Registrar may be entitled.</p>
					<p class="item">The Registrar also reserves the right to cancel, transfer or otherwise make changes to the Domain Name registration in accordance with other legal requirements.</p>
				</div>
			</div>
			<div class="item"><h4>Limitation of Liability</h4>
				<div class="list">
					<p class="item">The Registrant agrees that the Registrar shall have no liability to the Registrant for any loss or damages:</p>
						<p>(i) in connection with the Registrar's processing of the Domain Name Application; or</p>
						<p>(ii) in connection with the Registrar's processing of any authorized modification to the Domain Name record during the period of any registration; or</p>
						<p>(iii) resulting from the refusal of the Registrar to accept any application for registration (save to refund any fee paid by the Registrant to the Registrar); or</p>
						<p>(iv) as the result of any failure on the part of the Registrant's agent to pay either the Registration Fee or the Renewal Fee; or</p>
						<p>(v) as a result of the application of the Dispute Policy.</p>
					<p class="item">The Registrant agrees that in no circumstances shall the Registrar be liable for any loss of profit, loss of business or anticipated savings suffered by the Registrant howsoever incurred.</p>
					<p class="item">The Registrant agrees that the Registrar shall not be liable for any loss of registration or loss of use of the Domain Name, or for interruption of business, or any indirect, special, incidental or consequential losses of any kind (including lost profits) whether in contract, tort (including negligence) or otherwise.</p>
					<p class="item">Without prejudice to the foregoing the Registrant agrees that any liability of the Registrar to the Registrant shall not exceed 10% of the Registration or Renewal Fees paid by the Registrant to the Registrar for the current period of registration.</p>
						<p>or</p><br/><p>The Registrant agrees that in no event shall the maximum liability of the Registrar to the Registrant for any matter exceed $100US.</p>
					<p class="item">The Registrant agrees that it is technically impracticable to provide services free of faults and the Registrar does not undertake to do so.</p>
				</div>
			</div>
			<div class="item"><h4>Indemnity</h4>
				<div class="list">
					<p class="item">The Registrant shall hold the Registrar and any of its directors, officers, employees and agents harmless from any claim by a third party arising out of or in connection with (i) the registration or use of a domain name or any other listing information, or (ii) the implementation by the Registrar of any order or decision referred to in Article [ ] of the Dispute Policy. Such claims shall include, without limitation, those based upon intellectual property trademark or service mark infringement, tradename infringement, dilution, tortious interference with contract or prospective business advantage, unfair competition, defamation or injury to business reputation. Such obligation shall continue in effect after the termination of this Registration Agreement.</p>
					<p class="item"> The Registrar recognizes that certain educational and government entities may not be able to provide indemnification. If the Registrant is (i) a governmental or non-profit educational entity and (ii) not permitted by law or under its organizational documents to provide indemnification, the Registrant must notify the Registrar in writing before making payment to the Registrar and, upon receiving appropriate proof of such restriction, the Registrar shall endeavor to provide an alternative indemnification provision for such Registrant.</p>
					<p class="item">[The Registrant agrees to indemnify, defend and hold harmless the Registry Administrator, its directors, officers, employees and agents from and against any and all claims, damages, liabilities, costs and expenses, including reasonable legal fees and expenses arising out of or relating to the registration of the Domain Name.]</p>
				</div>
			</div>
			<div class="item"><h4>Intellectual Property Rights</h4>
				<p>The Registrar does not accept any responsibility for the use of the Domain Name or information generally held in the Registry and, in particular, for any conflict with trademarks, registered or unregistered, or with any other intellectual property rights</p>
			</div>
			<div class="item"><h4> Modification or Surrender of Domain Name</h4>
				<p>The Registrant (either directly or via an agent) may modify or surrender the registration of the Domain Name in accordance with the procedures put in place by the Registrar for such purposes.</p>
			</div>
			<div class="item"><h4> Termination</h4>
				<div class="list">
					<p class="item">The Registrant may terminate this Registration Agreement before its expiration by requesting the Registrar to delete the Domain Name from the Registry in accordance with the procedures established by the Registrar for such purposes.</p>
					<p class="item">The Registrar may terminate this Registration Agreement before its expiration in the event that the Registrar determines, in its sole discretion, that there was a material misrepresentation, material inaccuracy or materially misleading statement in the information provided in the Domain Name Application or in any subsequent modification thereof.</p>
					<p class="item">Termination of the Registration Agreement shall not determine rights and obligations between the parties that are of a continuing nature nor shall modification, surrender, cancellation or transfer of the Domain Name extinguish any rights that have accrued under the terms of the Registration Agreement.</p>
				</div>
			</div>
			<div class="item"><h4>Assignment</h4>
				<p>The Registrar may assign the Registration Agreement. The Registrant may assign the Registration Agreement subject to any change made pursuant to Article [ ] below, and transfer the registration of the Domain Name, by strict adherence to the procedure in force at the time of transfer and payment of the appropriate transfer fee applicable at the time of the transfer. No other method of assignment is permitted.</p>
			</div>
			<div class="item"><h4>Renewal or Transfer</h4>
				<p>The Registrar may vary the terms and conditions of the Registration Agreement on renewal or transfer of the registration of the Domain Name. All assignments and renewals shall be pursuant to the Terms & Conditions current at the time of the transfer or renewal and, in the case of a transfer, as agreed by the Transferor.</p>
			</div>
			<div class="item"><h4>Governing Law</h4>
				<p>This Registration Agreement shall be governed in all respects by and construed in accordance with the laws of the Republic of Trinidad and Tobago.</p>
			</div>
			<div class="item"><h4>Jurisdiction</h4>
				<p>For the adjudication of disputes concerning or arising out of the registration and use of the Domain Name, the Registrant consents to submit, without prejudice to other potentially applicable jurisdictions, to the jurisdiction of the courts (i) of the Registrant's domicile, and (2) where the Registrar is located.</p>
			</div>
			<div class="item"><h4>Disputes between Registrar and Registrant</h4>
				<p>Any dispute, controversy or claim arising under, out of or relating to the Registration Agreement and any subsequent amendments of this Registration Agreement, including, without limitation, its formation, validity, binding effect, interpretation, performance, breach or termination, as well as non-contractual claims, shall be referred to and finally determined by arbitration in accordance with the WIPO Arbitration Rules. The language to be used in the arbitration proceedings shall be the language of the Registration Agreement.</p>
			</div>
			<div class="item"><h4>Notice</h4>
				<div class="list">
					<p class="item"> Except as otherwise provided in this clause any notice to be given under the Registration Agreement shall only be deemed to be served if delivered by hand or sent by pre-paid first class post, by fax or e-mail, to the party to whom it is given at its last known postal or e-mail address or fax number. The notice will be effective: if delivered by hand, on delivery; if sent by fax or e-mail, when the sender receives confirmation of receipt; and if sent by post, on the fourth day after posting.</p>
					<p class="item">Any notices to the Registrant concerning a dispute under the provisions of the Dispute Policy shall be validly delivered if sent by e-mail to the address of the Administrative Contact as specified in the Domain Name Application.</p>
				</div>
			</div>
			<div class="item"><h4>Non-Agency</h4>
				<p>Nothing contained in the Registration Agreement shall be construed as creating any agency, partnership or other form of joint enterprise between the Registrar and the Registrant or between the Registrant and a third party.</p>
			</div>
			<div class="item"><h4>Severability</h4>
				<p>In the event that any provision of the Registration Agreement shall be found to be unenforceable or invalid under the applicable law or be so held by any applicable decision of a Court, the remaining provisions shall not be affected and shall continue to be binding. The Registrar shall use its best endeavors within one (1) month of being notified that any such provision is unenforceable or invalid as aforesaid to substitute a valid</p>
			</div>
			<div class="item"><h4>Clause Headings</h4>
				<p>Clause headings are for ease of reference and are not part of this Agreement and accordingly shall not affect its interpretation.</p>
			</div>
			<div class="item"><h4>Entirety of Understanding</h4>
				<p>The Registrant agrees that this Registration Agreement is the complete and exclusive agreement between the Registrant and the Registrar regarding the registration of the Domain Name. This Registration Agreement supersedes all prior agreements and understandings, whether established by custom, practice, policy, or precedent. Except where provided otherwise in this Registration Agreement, no variation may be made to the Registration Agreement unless such is in writing and signed by a duly authorized representative of the Registrant and the Registrar.</p>
			</div>
		</div>
		<h4>END OF DOCUMENT</h4>
	</div>
@stop
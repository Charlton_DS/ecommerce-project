@extends('app')

@section('content')

    <div class="ui three column centered grid">
        <div class="column"></div>
        <div class="column">
            <h3 class="ui horizontal header divider">
                Domain Details for {{$domain->name.'.'.$domain->tld}}
            </h3>
            @if (count($errors) > 0)
                <div class="ui negative message"><!-- put in red-->
                    <strong>Whoops!</strong> Some of your information is not valid.
                </div>
            @endif
            {!! Form::open(['action' => array('DomainsController@update',$domain->id), 'method' => 'put', 'class' => 'ui form']) !!}

            @if(isset($admin) && $admin)
            <h4 class="ui dividing header">Registrant Information</h4>
                <div class="disabled field">
                    <label>Registrant Name</label>
                    {!! Form::text('', $domain->registrant,['placeholder' => 'Registrant Name', 'disabled' => ''])!!}
                </div>
                <div class="field">
                    <label>Registrant Address</label>
                    {!! Form::textArea('addr',$domain->registrant_addr,['placeholder' => $domain->registrant_addr, 'required'])!!}
                </div>
                <div class="disabled field">
                    <label>Admin Contact</label>
                    {!! Form::text('admin',$domain->admin,['placeholder' => 'Business Address','disabled' => ''])!!}
                </div>
                <div class="field">
                    <label>Billing Contact</label>
                    {!! Form::text('billing',$domain->billing,['placeholder' => $domain->billing, 'required'])!!}
                </div>
                <div class="field">
                    <label>Technical Contact</label>
                    {!! Form::text('technical',$domain->tech,['placeholder' => $domain->tech, 'required'])!!}
                </div>
            @endif
            <h4 class="ui dividing header">DNS Information</h4>

            <div class="two fields">
                <div class="field">
                    <label>Hostnames</label>
                    @if(isset($domain->dns_hosts))
                        {!! Form::textArea('hosts',$domain->dns_hosts,['placeholder' => 'DNS Hostnames', 'required','data-content' => 'Add each DNS Hostname on a separate line'])!!}
                    @else
                        {!! Form::textArea('hosts',null,['placeholder' => 'DNS Hostnames', 'required', 'data-content' => 'Add each DNS Hostname on a separate line'])!!}
                    @endif
                </div>
                <div class="field">
                    <label>IP Addressed</label>
                    @if(isset($domain->dns_ips))
                        {!! Form::textArea('ips',$domain->dns_ips,['placeholder' => 'IP Addresses', 'required', 'data-content' => 'Add each DNS IP Address on a separate line'])!!}
                    @else
                        {!! Form::textArea('ips',null,['placeholder' => 'IP Addresses', 'required', 'data-content' => 'Add each DNS IP Address on a separate line'])!!}
                    @endif
                </div>
            </div>
            <h4 class="ui dividing header"></h4>
            <div class="field">
                <button class="ui labeled positive icon button" type="submit">
                    <i class="checkmark icon"></i>
                    Submit
                </button>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="column"></div>


    </div>
@stop
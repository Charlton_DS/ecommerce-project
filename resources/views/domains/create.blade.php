@extends('app')

@section('content')

    <h1>Domain Registration</h1>

    {!! Form::open(['action' => 'CartController@store']) !!}

        {!! Form::label('Domain Name') !!}
        {!! Form::text('name',null,['required'])!!}

        {!! Form::label ('TLD') !!}
        {!! Form::select('tld', array('tt' => '.tt',
                                        'co.tt' => '.co.tt',
                                        'com.tt' => '.com.tt',
                                        'org.tt' => '.org.tt',
                                        'net.tt' => '.net.tt',
                                        'biz.tt' => '.biz.tt',
                                        'pro.tt' => '.pro.tt',
                                        'info.tt' => '.info.tt',
                                        'name.tt' => '.name.tt',)
                                        , '.tt') !!}

        {!! Form::submit('Send Application') !!}
    {!! Form::close() !!}

    @if(isset($cart))
        @include('cart.cart', ['cart' => $cart])
    @endif
@stop


@extends('app')

@section('content')

    <h1>Domains</h1>

    @foreach($domains as $domain)
        <h4>{{$domain->name}}</h4>
    @endforeach

@stop

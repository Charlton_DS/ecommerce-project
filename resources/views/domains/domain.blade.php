@extends('app')

@section('content')
    <?php use Illuminate\Support\Facades\Auth;use libphonenumber\PhoneNumberUtil; ?>
    <div class="ui three column centered grid">
        <div class="column"></div>
        <div class="column">
            <h3 class="ui horizontal header divider">
                Domain Details for {{$domain->name.'.'.$domain->tld}}
            </h3>
            <h5 class="ui top attached header inverted purple segment">
                Registrant Details
            </h5>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Registrant Name: </b>
                    </div>
                    <div class="column">
                        {{$domain->registrant}}
                    </div>
                </div>
            </div>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Registrant Address: </b>
                    </div>
                    <div class="column">
                        {{$domain->registrant_addr}}
                    </div>
                </div>
            </div>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Expiration Date: </b>
                    </div>
                    <div class="column">
                        {{$domain->expire->diffForHumans()}}<br>({{$domain->expire->toFormattedDateString()}})
                    </div>
                </div>
            </div>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Activated Domain: </b>
                    </div>
                    <div class="column">
                        @if($domain->activated)
                            <p>Yes</p>
                        @else
                            <p>No</p>
                        @endif
                    </div>
                </div>
            </div>
            <h5 class="ui attached header inverted purple segment">
                Contact Information
            </h5>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Admin Contact:</b>
                    </div>
                    <div class="column">
                        {{$domain->owner->name}}<br>
                        {!! HTML::mailto($domain->owner->email) !!}
                        @if(isset($domain->owner->phone))
                            <?php
                            $phoneUtil = PhoneNumberUtil::getInstance();
                            $phone = $phoneUtil->parse($domain->owner->phone, $domain->owner->country);
                            if(Auth::check()){
                                $phone = $phoneUtil->formatOutOfCountryCallingNumber($phone, Auth::user()->country);
                            }
                            else{
                                $phone = $phoneUtil->formatOutOfCountryCallingNumber($phone, 'TT');
                            }
                            ?>
                            <br>{{$phone}}
                        @endif
                    </div>
                </div>
            </div>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Billing Contact:</b>
                    </div>
                    <div class="column">
                        {{$domain->bill->name}}<br>
                        {!! HTML::mailto($domain->bill->email) !!}
                        @if(isset($domain->bill->phone))
                            <?php
                            $phoneUtil = PhoneNumberUtil::getInstance();
                            $phoneb = $phoneUtil->parse($domain->bill->phone, $domain->bill->country);
                            if(Auth::check()){
                                $phoneb = $phoneUtil->formatOutOfCountryCallingNumber($phoneb, Auth::user()->country);
                            }
                            else{
                                $phoneb = $phoneUtil->formatOutOfCountryCallingNumber($phoneb, 'TT');
                            }

                            ?>
                            <br>{{$phoneb}}
                        @endif
                    </div>
                </div>
            </div>
            <div class="ui attached segment">
                <div class="ui two column grid">
                    <div class="column">
                        <b>Technical Contact:</b>
                    </div>
                    <div class="column">
                        {{$domain->technical->name}}<br>
                        {!! HTML::mailto($domain->technical->email) !!}
                        @if(isset($domain->technical->phone))
                            <?php
                            $phoneUtil = PhoneNumberUtil::getInstance();
                            $phonet = $phoneUtil->parse($domain->technical->phone, $domain->technical->country);
                            if(Auth::check()){
                                $phonet = $phoneUtil->formatOutOfCountryCallingNumber($phonet, Auth::user()->country);
                            }
                            else{
                                $phonet = $phoneUtil->formatOutOfCountryCallingNumber($phonet, 'TT');
                            }
                            ?>
                            <br>{{$phonet}}
                        @endif
                    </div>
                </div>
            </div>
            <h5 class="ui attached header inverted purple segment">
                DNS Information (Host | Ip)
            </h5>
            @for($i=0; $i < count($hosts); $i++)
                <div class="ui attached segment">
                    <div class="ui two column grid">
                        <div class="column">
                            {{$hosts[$i]}}
                        </div>
                        <div class="column">
                            {{$ips[$i]}}
                        </div>
                    </div>
                </div>
            @endfor

            @if(Auth::check())
                @if(Auth::user()->id == $domain->owner->id || Auth::user()->id == $domain->technical->id)
                    <div class="ui hidden divider"></div>

                    {!! Form::open(['action' => array('DomainsController@edit',$domain->id), 'method' => 'get']) !!}
                    <button class="ui labeled positive icon button">
                        <i class="edit icon"></i>
                        Edit Domain
                    </button>
                    {!! Form::close() !!}
                @endif

                @if(Auth::user()->isAdmin)
                    <div class="ui hidden divider"></div>

                    <div class="ui two column centered grid">
                        <div class="column">
                            {!! Form::open(['action' => array('AdminController@toggleActivation'), 'class' => 'ui form']) !!}
                            {!! Form::hidden('id', $domain->id) !!}
                            @if($domain->activated)
                                <button class="ui negative button" type="submit">
                                    Deactivate
                                </button>
                            @else
                                <button class="ui positive button" type="submit">
                                    Activate
                                </button>
                            @endif
                            {!! Form::close() !!}
                        </div>
                        <div class="column">
                            {!! Form::open(['action' => array('AdminController@extend'), 'class' => 'ui form']) !!}
                            {!! Form::hidden('id', $domain->id) !!}

                            <div class="ui left action input">
                                <button class="ui positive button" type="submit">
                                    Extend
                                </button>
                                {!! Form::input('number','months', null , ['placeholder' => 'Months', 'min' => '1', 'max' => '6', 'step' => '1', 'required']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @endif

            @endif
        </div>


        <div class="column">
            @if(Auth::check())

            @endif

        </div>
    </div>
@stop
@extends('app')

@section('content')

<br/>
<br/>
<p style="font-size:20px; margin-left:40px">Check which rates are in your radar</p>
<div style="margin-left:40px; width:1200px"class="ui segment">
	<div class="ui two column grid">
		<div class="twelve wide column">
			<div class="ui horizontal segment">
		 	<div class="ui striped table">
		 		<table>
		 		 <thead>
      				<tr>
        				<th>FEES</th>
        				<th>3rd level (e.g., name.co.tt)</th>
        				<th>multi-character 2nd level domain (e.g. name.tt)</th>
        				<th>single-character 2nd level domain (e.g. z.tt)</th>
      				</tr>
    			</thead>
		 		<tbody>
		 			<tr>
		 				<td>Registration Fees</td>
		 				<td id="values">$300TT (covers 3 years)</td>
		 				<td id="values2">$2000TT (covers 3 years)</td>
		 				<td id="values3">$6000TT (covers 3 years)</td>
		 			</tr>
		 			<tr>
		 				<td>Maintenance Fees</td>
		 				<td id="values4">$300TT every 3 years (from 4th year)</td>
		 				<td id="values5">$2000TT every 3 years (from 4th year)</td>
		 				<td id="values6">$6000TT every 3 years (from 4th year)</td>
		 			</tr>
		 			<tr>
		 				<td>Domain Name Change</td>
		 				<td>Same as registration of a new domain</td>
		 				<td>Same as registration of a new domain</td>
		 				<td>Same as registration of a new domain</td>	
		 			</tr>
		 		</tbody>
		 		</table>
		 	</div>
		 	</div>
		</div>
		<div class=" two wide column">
		  <div class="ui horizontal segment">
		  		<p style="font-size:20px;text-align:right">Currency</p>
		  		<div class="ui green basic huge animated button" onclick="check()">    				
      				<div class="visible content" id="curr">$TT</div>
      				<div class="hidden content"  id="curr2">$US</div>
    			</div>
		  </div>
		</div>
	</div>
	<br/>
	<br/>
	<div class="ui red segment">
	    <p style="font-size:20px">Important Notes:</p>
		<div class="ui bulleted divided list">
  			<div class="item" style=padding:5px>
  				<p>We accept Credit Cards, Cheques, Cash or <a href="#">Wire Transfers</a>.</p>
  			</div>
  			<div class="item" style=padding:5px>
  				<p>Please note the maintenance period covered by the registration fee.</p>
  			</div>
  			<div class="item" style=padding:5px>
  				<p>Registration and maintenance invoices are e-mailed to the billing address that is provided by the applicant.</p>
  			</div>
  			<div class="item" style=padding:5px>
  				<p>Domains are activated upon receipt of payment.</p>
  			</div>
  			<div class="item" style=padding:5px>
  				<p>All changes to your domain application information are free of charge <b>except changes to the domain name</b>.</p>
  			</div>
  			<div class="item" style=padding:5px>
  				<p>All billing and account questions should be sent to the TTNIC Billing administrator at <a href="#">billing@nic.tt</a></p>
  			</div>
  		</div>	
  	
</div>
<script>

	
	function check(){
	 	var curr = 	document.getElementById("curr");
	 	var curr2 = 	document.getElementById("curr2");
	 	
	 	if(curr.innerHTML=="$TT"){
	        curr.innerHTML = "$US";
	        curr2.innerHTML = "$TT";
	        
	       
	        document.getElementById("values").innerHTML = "$100US (covers 3 years)";
	        document.getElementById("values2").innerHTML = "$600US (covers 3 years)";
	        document.getElementById("values3").innerHTML = "$2000US (covers 3 years)";
	        
	         document.getElementById("values4").innerHTML = "$100US every 3 years (from 4th year)";
	         document.getElementById("values5").innerHTML = "$600US every 3 years (from 4th year)";
	         document.getElementById("values6").innerHTML = "$2000US every 3 years (from 4th year)";
	    }
	    else{
	    	curr.innerHTML = "$TT";
	    	curr2.innerHTML = "$US";
	    	
	    	document.getElementById("values").innerHTML = "$300TT (covers 3 years)";
	        document.getElementById("values2").innerHTML = "$2000TT (covers 3 years)";
	        document.getElementById("values3").innerHTML = "$6000TT (covers 3 years)";
	        
	        document.getElementById("values4").innerHTML = "$300TT every 3 years (from 4th year)";
	        document.getElementById("values5").innerHTML = "$2000TT every 3 years (from 4th year)";
	        document.getElementById("values6").innerHTML = "$6000TT every 3 years (from 4th year)";
	    }
	    
	}
	
</script>
@stop
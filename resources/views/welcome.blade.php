 
@extends('app')

@section('content')



    
    <div class="ui grid">

		<div class="three column row">
			<div class="two wide column"></div>

			<div class="twelve wide column">
				
				<div style='background-image: url("p_flat.png"); min-height: 250px;'>
					<div style="padding-top:100px; padding-bottom:100px; padding-left:250px; padding-right:250px;">
						<h2 class="ui center aligned inverted header">
							Trinidad and Tobago<br> Network Information Centre
							<div class="sub header">
								Claim Your Name!
							</div>
						</h2>
						<div class="ui hidden divider"></div>
						{!! Form::open(['action' => 'SearchController@search', 'class' => 'ui form']) !!}
						<div class="ui icon input">
							<input type="text" name= "term" placeholder="Search Domains" autofocus required>
							<i class="search icon"></i>
						</div>
						{!! Form::close() !!}
				    </div>
				</div>
				<div class="ui hidden divider"></div>
				<div class="ui three column grid">
				  <div class="column">
				    <div class="ui fluid card">
				      <div class="content">
				        <a class="header">About Us</a>
				        <div class="description">
				        	<p>The Trinidad and Tobago Network Information Centre (TTNIC) manages the registration of domain names within the TT (Trinidad and Tobago) Top Level Domain.
					 		</p>
					 		<p>Its management is advised by, and policies developed by, a <a href="#">Multi-Stakeholder Advisory Group</a> which has representatives from major local Internet stakeholders. </p>
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="column">
				    <div class="ui fluid card">
				      <div class="content">
				        <a class="header">Domains Offered</a>
				        <div class="description">
				        	<p>Single and Multiple Character Second Level Domains (e.g.,<b> a.tt</b> and <b>companyname.tt</b>).</p>
							 <p>Third Level Domains under <b>co.tt, com.tt, org.tt, net.tt, biz.tt, pro.tt, info.tt, and name.tt</b>.</p>
							 <p>Third Level Domains under <b>gov.tt</b> managed by the Government of Trinidad and Tobago.</p><br>
				    	</div>
				      </div>
				    </div>
				  </div>
				  <div class="column">
				    <div class="ui fluid card">
				      <div class="content">
				        <a class="header">Policies</a>
				        <div class="description">
				        	<p>Registrants must agree to be bound by our <a href="#">Terms and Conditions</a> and <a href="/policy" >Policies</a>.</p> 
						<p>Registrants agree to accept the <a href="#">Uniform Dispute Resolution Policy</a>.</p> 
						<p>Please pay special attention to actions we take when harmful web sites are hosted under any of our domain names.</p>
				        </div>
				      </div>
				      </div>
				    </div>
				  </div>
				  <div class="ui hidden divider"></div>
				  <h2 class="ui blue  header">Registration Process</h2>
				  <div class="ui hidden divider"></div>
				  <div class="ui grid">
				    <div class="row">
				      <div class="two wide column">
				        <h2 class="ui header">
						</h2>
				      </div>
				      <div class="fourteen wide column">
				        <h3 class="ui dividing header"> 
				        	<i class="search circular inverted teal icon"></i>
				        	<div class="content">
				        		Search
							   <div class="sub header">
								Search for desired domain
							</div>
							  </div>
						</h3>	
				      </div>
				    </div>
				    <div class="row">
				      <div class="four wide column">
				        <h2 class="ui header">
						</h2>
				      </div>
				      <div class="twelve wide column">
				        <h3 class="ui dividing header"> 
				        	<i class="paint brush circular inverted teal icon"></i>
				        	<div class="content">
				        		Mark for Purchase
							   <div class="sub header">
								Add domain to cart.
							</div>
							  </div>
						</h3>	
				      </div>
				    </div>
				    <div class="row">
				      <div class="six wide column">
				        <h2 class="ui header">
						</h2>
				      </div>
				      <div class="ten wide column">
				        <h3 class="ui dividing header"> 
				        	<i class="send circular inverted teal icon"></i>
				        	<div class="content">
				        		Submit
							   <div class="sub header">
								Submit for approval.
							</div>
							  </div>
						</h3>	
				      </div>
				    </div>
				    <div class="row">
				      <div class="eight wide column">
				        <h2 class="ui header">
						</h2>
				      </div>
				      <div class="eight wide column">
				        <h3 class="ui dividing header"> 
				        	<i class="mail circular inverted teal icon"></i>
				        	<div class="content">
				        		Emails
							   <div class="sub header">
								Recieve Email with invoice
							</div>
							  </div>
						</h3>	
				      </div>
				    </div>
				    <div class="row">
				      <div class="ten wide column">
				        <h2 class="ui header">
						</h2>
				      </div>
				      <div class="six wide column">
				        <h3 class="ui dividing header"> 
				        	<i class="money circular inverted teal icon"></i>
				        	<div class="content">
				        		Payments
							   <div class="sub header">
								Make Payment via Stripe Checkout
							</div>
							  </div>
						</h3>	
				      </div>
				    </div>
				  </div>
			</div>
			<div class="two wide column"></div>
		</div>

	</div>

	<div class="ui hidden divider"></div>
	<footer>
		<div class="ui horizontal inverted purple segment">
			<div class="ui inverted link list">
				<p style="text-align:center">
					<a href="/aboutUs">About Us</a>&nbsp;&nbsp;&nbsp;
					<a href="/contactUs">Contact Us</a>&nbsp;&nbsp;&nbsp;
					<a href="/faq">FAQs</a>&nbsp;&nbsp;&nbsp;
					<a href="/fees">Fees</a>&nbsp;&nbsp;&nbsp;
					<a href="/policy">Policy</a>&nbsp;&nbsp;&nbsp;
					<a href="/termsAndConditions">Terms and Conditions</a>&nbsp;&nbsp;&nbsp;
				</p>
				<p style="text-align:center">&copy; Copyright 1995-2015 by TTNIC. All Rights Reserved.</p>
			</div>
		</div>
	</footer>
@stop



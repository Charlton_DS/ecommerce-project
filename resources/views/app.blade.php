<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TTNIC</title>

	<!--<link href="/css/app.css" rel="stylesheet">-->
	<link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.6/semantic.min.css" rel="stylesheet">
	<link href="css/index.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<div class="ui purple inverted large menu">
	<a class="item" href="/">
		<i class="home icon"></i> TTNIC
	</a>
	@if(!Auth::guest() && Auth::user()->isAdmin)
		<div class="ui dropdown item">
			<i class="unlock alternate icon"></i>
			Admin
			<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href={{action('AdminController@index',[Auth::user()->id])}}><i class="home icon"></i> Home</a>
				<a class="item" href={{action('AdminController@users')}}><i class="users icon"></i> Users</a>
				<a class="item" href={{action('AdminController@approve')}}><i class="check circle outline icon"></i> Approvals</a>
			</div>
		</div>
	@endif
	<div class="right menu">
		<div class="item" data-content="Search for Domain">
			<div class="ui inverted transparent icon input">
				{!! Form::open(['action' => 'SearchController@search']) !!}
				<input type="text" name= "term" placeholder="Search Domains" required>
				<i class="search icon"></i>
				{!! Form::close() !!}
			</div>
		</div>
		@if (Auth::guest())
			<a class="item" href="/auth/login"><i class="sign in icon"></i>Login</a>
			<a class="item" href="/auth/register"><i class="users icon"></i>Register</a>
		@else


			<a class="item" href="/home" data-content="Domains Purchased">
				<i class="globe icon"></i>
				{{$count = Auth::user()->adminDomains()->count()}}
			</a>

			<a class="item" href="{{action('CartController@index')}}" data-content="Domains in Cart">
				<i class="bookmark icon"></i>
				{{$count = Auth::user()->cart()->pending()->count()}}
			</a>

			<a class="item" href="{{action('PaymentController@index')}}" data-content="Pending Payments">
				<i class="payment icon"></i>
				{{$count = Auth::user()->payments()->unPaid()->count()}}
			</a>
			<div class="ui dropdown item">
				<i class="user icon"></i>
				{{ Auth::user()->name }}
				<i class="dropdown icon"></i>
				<div class="menu">
					<a class="item" href={{action('ProfileController@edit',[Auth::user()->id])}}><i class="edit icon"></i> Edit Profile</a>
					<a class="item" href={{action('SecondFactorController@enable')}}><i class="qrcode icon"></i> Second Factor Authentication</a>
					<a class="item" href="/auth/logout"><i class="sign out icon"></i> Sign out</a>
				</div>
			</div>
		@endif

	</div>
</div>

@yield('content')


<!--<footer style="padding-top:40px;">
    <div class="ui inverted purple segment">
        <div class="ui one column grid">
          <div class="ui bound bottom sticky">
          <div class="center aligned column">
              <div class="ui horizontal segment">
                  <div class="ui inverted link list">
                        <a href="#">About Us</a>&nbsp;&nbsp;&nbsp;
                        <a href="#">Contact Us</a>&nbsp;&nbsp;&nbsp;
                        <a href="#"> FAQs</a>&nbsp;&nbsp;&nbsp;
                        <a href="#">Policy</a>&nbsp;&nbsp;&nbsp;
                        <a href="#"> Terms and Conditions</a>&nbsp;&nbsp;&nbsp;
                        <p style="text-align:center">&copy; Copyright 1995-2015 by TTNIC. All Rights Reserved.</p>
                 </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</footer>-->






<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.6/semantic.min.js"></script>
<!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>-->
<script src="/app.js"></script>
</body>
</html>

{!! HTML::script('https://checkout.stripe.com/checkout.js', array(
        'class'             =>  'stripe-button',
        'data-key'          =>  'pk_test_qwaRKjMkzmSWhUZSI61QzXze',
        'data-amount'       =>  '',
        'data-email'        =>  Auth::user()->email,
        'data-name'         =>  'TTNIC',
        'data-description'  =>  'TTNIC Domain Fees',
        'data-image'        =>  '/128x128.png'
)) !!}

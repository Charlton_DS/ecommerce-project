@extends('app')

@section('content')

    <div class="ui three column centered grid">
        <div class="column"></div>
        <div class="column">
            @if(isset($message))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    {{$message}}
                </div>
            @endif
                @if (count($errors) > 0)
                    <div class="ui negative message"><!-- put in red-->
                        <strong>Whoops!</strong> There were some problems with your payment.
                        <div class="ui bulleted list">
                            @foreach ($errors->all() as $error)
                                <div class="item"> {{$error}} </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            @foreach($payments as $payment)
                <h5 class="ui top attached header inverted purple segment">
                    @if(count($payment->domains) == 1)
                        Payments for {{count($payment->domains)}} Domain
                    @else
                        Payments for {{count($payment->domains)}} Domains
                    @endif
                </h5>
                <div class="ui attached segment">
                    <div class="ui two column grid">
                        <div class="column">
                            <b>Domains: </b>
                        </div>
                        <div class="column">
                            @foreach($payment->domains as $domain)
                                {{$domain->name.'.'.$domain->tld}}<br>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="ui attached segment">
                    <div class="ui two column grid">
                        <div class="column">
                            <b>Amount:</b>
                        </div>
                        <div class="column">
                            @if($payment->amount_tt >0)
                                ${{$payment->amount_tt}}TTD<br>
                            @endif
                            @if($payment->amount_us >0)
                                ${{$payment->amount_us}}USD<br>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="ui attached segment">
                    <div class="ui two column grid">
                        <div class="column">
                            <b>Duration:</b>
                        </div>
                        <div class="column">
                            {!! Form::open(['class' => 'ui form']) !!}

                            {!! Form::hidden('id',$payment->id) !!}

                            <div class="grouped fields">
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        {!! Form::radio('years', '3', true) !!}
                                        <label>3 Years</label>
                                    </div>

                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        {!! Form::radio('years', '6', false) !!}
                                        <label>6 Years</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        {!! Form::radio('years', '9', false) !!}
                                        <label>9 Years</label>
                                    </div>
                                </div>
                            </div>

                            @if($payment->amount_tt == 0)
                                @include('payments.stripeemb', ['payment' => $payment, 'total' => $payment->amount_us.'US' ])
                            @elseif($payment->amount_us == 0)
                                @include('payments.stripeemb', ['payment' => $payment, 'total' => $payment->amount_tt.'TT'])
                            @else
                                @include('payments.stripeemb', ['payment' => $payment, 'total' => $payment->amount_tt.'TT'.' '.$payment->amount_us.'US'])
                            @endif

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            @endforeach

            @if(count($payments)==0)
                <h2 class="ui center aligned icon header">
                    <i class="smile icon"></i>
                    <div class="content">
                        You Have no Outstanding Payments
                    </div>
                </h2>
            @endif

        </div>
        <div class="column"></div>
    </div>


@stop

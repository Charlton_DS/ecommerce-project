<div class="orange card">
    <div class="content">
        <div class="header">{{$name}}.{{$tld->tld}}</div>
        <div class="meta">
            <span>Status:</span>
            Bought
        </div>
        @if($tld->tld === 'tt')
            <div class="meta">
                <span>Level:</span>
                2
            </div>
        @else
            <div class="meta">
                <span>Level:</span>
                3
            </div>
        @endif
        <div class="description">
            <p>This domain has already been taken by {{$result->registrant}}</p>
        </div>
    </div>
    {!! Form::open(['action' => array('DomainsController@show',$result->id), 'method' => 'get']) !!}
    <button class="fluid ui bottom attached button" type="submit">
        <i class= "list layout icon"></i>
        View Domain
    </button>
    {!! Form::close() !!}
</div>
<div class="green card">
    <div class="content">
        <div class="header">{{$name}}.{{$tld->tld}}</div>
        @if($exist)
            @if($sub)
                <div class="ui orange corner label"><i class="large check circle outline icon"></i></div>
            @else
                <div class="ui blue corner label"><i class="large check circle outline icon"></i></div>
            @endif
        @endif
        <div class="meta">
            <span>Status:</span>
            Available
        </div>
        @if($tld->tld === 'tt')
            <div class="meta">
                <span>Level:</span>
                2
            </div>
        @else
            <div class="meta">
                <span>Level:</span>
                3
            </div>
        @endif
        <div class="description">
            <p>Fees are based on the location of applicant. </p>
        </div>
    </div>
    @if($exist)
        @if($sub)
            <button class="fluid ui disabled bottom attached button" type="submit">
                <i class= "large remove bookmark icon"></i>
                Pending Approval
            </button>
        @else
            {!! Form::open(['action' => 'CartController@delete']) !!}
            {!! Form::hidden('id',$id)!!}
            <button class="fluid ui bottom attached button" type="submit">
                <i class= "large remove bookmark icon"></i>
                Unmark Domain
            </button>
            {!! Form::close() !!}
        @endif
    @else
        {!! Form::open(['action' => 'CartController@store']) !!}
        {!! Form::hidden('name',$term)!!}
        {!! Form::hidden('tld',$tld->tld)!!}
        <button class="fluid ui bottom attached button" type="submit">
            <i class= "large bookmark icon"></i>
            Mark for Purchase
        </button>
        {!! Form::close() !!}
    @endif

</div>

@extends('app')

@section('content')

    <h3 class="ui horizontal header divider">
        <i class="grid layout icon"></i>
        Results
    </h3>
    <div class="ui two column centered grid">
        <div class="column">
            <div class="ui three doubling cards">
                @foreach($tlds as $tld)
                    {{$exist = false}}
                    {{$exist_cart = false}}
                    {{$sub = false}}
                    @foreach($results as $result)
                        @if($result->tld == $tld->tld)
                            <?php $exist = true; ?>

                        @endif
                    @endforeach
                    @if($exist)
                        @include('search.unavailable',['name' => $term, 'tld' => $tld,'domain' => $result])
                    @elseif(!$exist)
                        @if(isset($cart))
                            @foreach($cart as $item)
                                @if($item->tld == $tld->tld && $item->name == $term)
                                    <?php $exist_cart = true; $id = $item->id?>
                                    @if($item->submitted)
                                        <?php $sub = true;?>
                                    @endif
                                @endif
                            @endforeach
                            @if($exist_cart)
                                @include('search.available',['name' => $term, 'tld' => $tld, 'exist'=>$exist_cart, 'id' => $id, 'sub' => $sub])
                            @else
                                @include('search.available',['name' => $term, 'tld' => $tld])
                            @endif
                        @else
                            @include('search.available',['name' => $term, 'tld' => $tld])
                        @endif

                        <div class="item">
                            {!! Form::open(['action' => 'CartController@store']) !!}
                            {!! Form::hidden('name',$term)!!}
                            {!! Form::hidden('tld',$tld->tld)!!}


                            <!--<button class="ui right labeled icon button cart_width" type="submit">
							   $60.00 {{$term}}.{{$tld->tld}}
								<i class= "large add to cart icon link icon"></i>
							</button>-->

                            {!! Form::close() !!}
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </div>
    </div>
    </div>
    @if(Auth::check())
        <h4 class="ui horizontal header divider">
            <a href="{{action('CartController@index')}}" class="item">
                <i class="send outline icon"></i>
                Submit Marked Domains
            </a>
        </h4>
    @endif
@stop

@extends('app')


@section('content')
    <div class="ui grid">

    <div class="twelve wide centered column" >
        <div>
            <center>
                <h2 class="ui header">
                    Domain Map
                    <div class="sub header">This is Map Show the Domain Distribution for Our Domains</div>
                </h2>
            </center>
        </div>
        <div id="pop-div"></div>
        @geochart('Popularity', 'pop-div')
    </div>

    </div>

@stop
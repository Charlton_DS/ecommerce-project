@extends('app')

@section('content')
    <?php use Illuminate\Support\Facades\Auth;use libphonenumber\PhoneNumberUtil; ?>
    <div class="ui grid">
        <div class="three column row">
            <div class="five wide column"></div>
            <div class="six wide column">

                @foreach($users as $user)
                    <div class="ui segments">
                        <div class="ui inverted teal segment">
                            {{$user->name}}
                        </div>
                        <div class="ui secondary segment">
                            <p><b>Email: </b> {!! HTML::mailto($user->email) !!}</p>
                            <p><b>Address: </b> {{$user->address}}</p>
                            <p><b>Phone: </b>
                                <?php
                                $phoneUtil = PhoneNumberUtil::getInstance();
                                $phone = $phoneUtil->parse($user->phone, $user->country);
                                $phone = $phoneUtil->formatOutOfCountryCallingNumber($phone, $user->country);
                                ?>
                                {{$phone}}
                            </p>
                            <div class="ui accordion field">
                                <div class="title">
                                    <i class="icon dropdown"></i>
                                    Domains
                                    <div class="ui teal label">{{count($user->adminDomains()->get())}}</div>
                                </div>
                                <div class="content ">
                                    <div class="ui blue labels">
                                        @foreach($user->adminDomains()->get() as $domain)
                                            {!! HTML::linkAction('DomainsController@show', $domain->name.'.'.$domain->tld,['id' => $domain->id], ['class' => 'ui label']) !!}
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                <div class="five wide column"></div>
            </div>

        </div>
@stop

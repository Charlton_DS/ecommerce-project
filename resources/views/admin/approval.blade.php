@extends('app')

@section('content')
    <?php use Illuminate\Support\Facades\Auth;use libphonenumber\PhoneNumberUtil;$start = 0 ?>
    <div class="ui three column centered grid">
        <div class="column"></div>
        <div class="column">
            <div class="ui accordion">

                @foreach($users as $user)
                    <div class="title">
                        <h4 class="ui dividing header">
                            <i class="large dropdown icon"></i>
                            {{$user->customer()->first()->name}}<div class="ui teal circular label">{{count($domains)}}</div>
                            <div class="sub header"><i class="mail outline icon"></i> {{$user['user']}}</div>
                        </h4>

                    </div>
                    <div class="content">
                        {!! Form::open(['action' => 'AdminController@approve', 'class' => 'ui form']) !!}
                        @for ($i = $start; $i < count($domains); $i++)
                            @if($domains[$i]['user'] != $user['user'])
                                <?php
                                $start = $i;
                                $i = count($domains);
                                ?>

                            @else
                                {!! Form::hidden('approved['.$domains[$i]->id.'][id]',$domains[$i]->id) !!}
                                <div class="ui segments">
                                    <div class="ui compact segment">
                                        <h5>Domain: {{strtolower($domains[$i]->name.'.'.$domains[$i]->tld)}}</h5>
                                        <small><em>Created {{$domains[$i]->created_at->diffForHumans()}}</em></small>
                                    </div>
                                    <div class="ui secondary segment">
                                        <p>Registrant Name: {{$domains[$i]->full_name}}</p>
                                        <p>Registrant Address: {{$domains[$i]->addr_1}}</p>
                                        <?php
                                        $phoneUtil = PhoneNumberUtil::getInstance();
                                        $phone = $phoneUtil->parse($domains[$i]->customer->phone, $domains[$i]->customer->country);
                                        $phone = $phoneUtil->formatOutOfCountryCallingNumber($phone, Auth::user()->country);

                                        ?>
                                        <p>Phone: {{$phone}}</p>
                                    </div>
                                </div>

                                <div class="field">
                                    {!! Form::hidden('approved['.$domains[$i]->id.'][approved]',0) !!}
                                    <div class="ui toggle checkbox">
                                        <label>Mark for <b>approval</b></label>
                                        {!! Form::checkbox('approved['.$domains[$i]->id.'][approved]',1)!!}
                                    </div>
                                </div>
                                <div class="field">
                                    {!! Form::hidden('approved['.$domains[$i]->id.'][local]',0) !!}
                                    <div class="ui toggle checkbox">
                                        <label>Mark as <b>local</b></label>
                                        {!! Form::checkbox('approved['.$domains[$i]->id.'][local]')!!}
                                    </div>
                                </div>
                                <div class="ui hidden divider"></div>

                            @endif
                        @endfor
                        <button class="ui positive button" type="submit">
                            Approve
                        </button>
                        {!! Form::close() !!}
                    </div>
                @endforeach

            </div>
            @if(!count($domains))
                <div>
                    <h2 class="ui center aligned icon header">
                        <i class="info circle icon"></i>
                        <div class="content">
                            No Submitted Domains
                            <div class="sub header">There seems to be no submitted domains</div>
                        </div>
                    </h2>
                    <center>
                        {{\Illuminate\Foundation\Inspiring::quote()}}
                    </center>
                </div>
            @endif
        </div>
        <div class="column"></div>
@stop
<div>
    <h4>Cart</h4>
    {!! Form::open(['action' => 'CartController@submit']) !!}
        {!! Form::submit('Submit') !!}
    {!! Form::close() !!}
    @foreach($cart as $item)
        <li>Domain: {{$item->name}}<br>
            Tld: {{$item->topLevelDomain->tld}}<br>
            @if($item->topLevelDomain->tld == 'tt')
                Level: 2nd
            @endif
            @if($item->topLevelDomain->tld != 'tt')
                Level: 3rd
            @endif
            Cost: {{$item->topLevelDomain->cost_tt}}
        </li>
    @endforeach
</div>
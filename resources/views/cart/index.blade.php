@extends('app')

@section('content')
    @if(Auth::user()->country == "TT")// This is to check to see if the user's IP is from Trinidad and Tobago
        <script language="Javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script language="Javascript">
            $.getJSON("http://jsonip.com?callback=?", function (data) {
                //list of IP Ranges in Trinidad
            if(!("64.28"==data.ip.substring(0,5) ||
                "181.188"==data.ip.substring(0,7) ||
                 "186.4" ==data.ip.substring(0,5)
                ))alert("Your IP address appears to be coming from outside Trinidad and Tobago, If you are found Violating the Policies then you may have your domain revoked and have no refund");
        });
        </script>
    @endif

    <div class="ui three column centered grid">
        <div class="column"></div>
        <div class="column">
            @if(isset($message))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    {{$message}}
                </div>
            @endif

            @if(isset($items))
                @if(count($items))
                    <div class="ui styled fluid accordion">
                        {!! Form::open(['action' => 'CartController@submit', 'class' => 'ui form']) !!}
                        @foreach($items as $item)


                            <div class="title">
                                <i class="large dropdown icon"></i>
                                <b>Domain: {{$item->name.'.'.$item->tld}}</b>
                            </div>

                            <div class="content">
                                <h4 class="ui dividing header">Registrant Information</h4>
                                <div class="required field">
                                    <label>Name</label>
                                    {!! Form::text('domain['.$item->id.'][name]',Auth::user()->name,['placeholder' => 'Registrant Name', 'required' => ''])!!}
                                </div>
                                <!--<div class="required inline field">
                                    <label>Phone Number</label>
                                    {!! Form::text('domain['.$item->id.'][phone]',Auth::user()->phone,['placeholder' => 'Contact number', 'required' => ''])!!}
                                </div>-->
                                <div class="required field">
                                    <label>Address</label>
                                    {!! Form::textArea('domain['.$item->id.'][address]',Auth::user()->address,['placeholder' => 'Business Address', 'required' => ''])!!}
                                </div>

                                <div class="field">
                                    <label>Country</label>
                                    <select name="domain[{{$item->id}}][country]" class="ui search dropdown">
                                        @foreach($countries as $country)
                                            @if($country->iso_3166_2== Auth::user()->country)
                                                <option selected value={{$country->iso_3166_2}} ><i class="{{strtolower($country->iso_3166_2)}} flag"></i> {{$country->name}}</option>
                                            @else
                                                <option value={{$country->iso_3166_2}}><i class="{{strtolower($country->iso_3166_2)}} flag"></i> {{$country->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="accordion">

                                    <div class="title">
                                        <i class="small dropdown icon"></i>
                                        Advanced Options
                                    </div>

                                    <div class="content">
                                        <div class="field">
                                            <label>Billing Contact</label>
                                            {!! Form::text('domain['.$item->id.'][billing]',$item->billing,['placeholder' => $item->billing, 'required'])!!}
                                        </div>

                                        <div class="field">
                                            <label>Technical Contact</label>
                                            {!! Form::text('domain['.$item->id.'][technical]',$item->technical,['placeholder' => $item->technical, 'required'])!!}
                                        </div>

                                        <div class="field">
                                            <label>DNSSEC</label>
                                            {!! Form::textArea('domain['.$item->id.'][dnssec]',null,['placeholder' => 'DS Record'])!!}
                                        </div>

                                        <div class="two fields">
                                            <div class="field">
                                                <label>Hostnames</label>
                                                {!! Form::textArea('domain['.$item->id.'][hosts]',null,['placeholder' => 'DNS Hostnames', 'data-content' => 'Add each DNS Hostname on a separate line'])!!}
                                            </div>
                                            <div class="field">
                                                <label>IP Addresses</label>
                                                {!! Form::textArea('domain['.$item->id.'][ips]',null,['placeholder' => 'IP Addresses', 'data-content' => 'Add each DNS IP Address on a separate line'])!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    <div class="ui hidden divider"></div>
                    <button class="ui positive button" type="submit">
                        Submit
                    </button>
                    {!! Form::close() !!}
                @else
                    <div>
                        <h2 class="ui center aligned icon header">
                            <i class="frown icon"></i>
                            <div class="content">
                                No Marked Domains
                                <div class="sub header">You do not seem to have any marked domains.... Try searching for a domain.</div>
                            </div>
                        </h2>
                    </div>
                @endif

            @else
                <div>
                    <h2 class="ui center aligned icon header">
                        <i class="frown icon"></i>
                        <div class="content">
                            No Marked Domains
                            <div class="sub header">You do not seem to have any marked domains.... Try searching for a domain.</div>
                        </div>
                    </h2>
                </div>
            @endif
        </div>

        <div class="column"></div>
    </div>
@stop
@extends('app')

@section('content')

	<div class="ui three column centered grid">
		<div class="column"></div>
		<div class="column">
			<h1 class="ui icon header">
				<i class="circular inverted teal legal icon"></i>
				<div class="content">
					Policies
					<div class="sub header">You are bound to these policies while using TTNIC.</div>
				</div>
			</h1>
		</div>
		<div class="column"></div>
	</div>

	<div class="ui grid">

		<div class="three column row">
			<div class="four wide column"></div>

			<div class="eight wide column">
				<h3 class="ui dividing header"> Privacy
					<div class="sub header">
						This policy covers how we use your personal information.
					</div>
				</h3>
				<p>We take your privacy seriously and will take all measures to protect your personal information.
					Any personal information received will only be used to fill your order.
					We will not sell or redistribute your information to anyone.</p>

				<div class="ui hidden divider"></div>
				<h3 class="ui dividing header"> Refunds
					<div class="sub header">
						This policy covers our refund scheme.
					</div>
				</h3>
				<p>Our policy is not to offer refunds.
					This is due to costs, mostly administrative and Registry costs, incurred immediately upon registration of the domain name.
					This policy is also to prevent applicants from "domain tasting" and is a standard policy for ccTLD Registries.</p>

				<div class="ui hidden divider"></div>
				<h3 class="ui dividing header"> TT Domain Names
					<div class="sub header">
						This policy covers our policies on registered domain names.
					</div>
				</h3>
				<div class="ui inverted relaxed divided list">
					<div class="item" >
						<p>Domains that are used as URL shorteners must include filters to block shortcuts to inappropriate content. Any violation of this policy will result in the immediate removal of the concerned domain.</p>
					</div>
					<div class="item" >
						<p>Registrants with web sites that are discovered to be risky (i.e., contain browser exploits, phishing, excessive popups, trojans, sypware etc.), either directly by the TTNIC or through complaints from Internet users, will be given 24 hours to remove the offending code before their domain is taken out of service. A second violation could lead to the permanent deletion of the domain.</p>
					</div>
					<div class="item" >
						<p>Registrations are processed on a first come first served basis but we abide by International copyright rules.</p>
					</div>
					<div class="item" >
						<p>We do not support Registrars and so all registrations must be made through the TTNIC.</p>
					</div>
					<div class="item" >
						<p>Registrants are not restricted to have a local presence but those that do (i.e., have a local addresss) are provided discounted fees.</p>
					</div>
					<div class="item" >
						<p>All changes to registration details are free but we do not permit changes in the Registrant's name (except if a company has a name change)</p>
					</div>
					<div class="item" >
						<p>Domain names must include only letters, numbers, or hyphens ("-") and cannot have more than 63 characters. They cannot begin with a hyphen.</p>
					</div>
					<div class="item" >
						<p>Subdomains within edu.tt are restricted to educational institutions within Trinidad and Tobago.</p>
					</div>
					<div class="item" >
						<p>Applications for subdomains within gov.tt must be made directly with the Government of Trinidad and Tobago and hence must follow their policies.</p>
					</div>
					<div class="item" >
						<p>We reserve the right to remove domains that provide objectionable content and not to renew domains on expiration.</p>
					</div>
					<div class="item" >
						<p>We reserve the right to reject domain names that are deemed to be objectionable.</p>
					</div>
					<div class="item" >
						<p>Registrants must pay within 30 days of registration (or within 30 days of the expiration date in the case of renewals) else the domain may be deleted.</p>
					</div>
					<div class="item" >
						<p>The billing terms are provided on the invoice and pricing information is available at www.nic.tt.</p>
					</div>
					<div class="item" >
						<p>For each registered domain, we make available (on our web site) the name of the Registrant, the address of the Registrant, the email addresses of Administrative, Billing and Technical Contacts, the corresponding nameserver information and the expiration date of the domain.</p>
					</div>
				</div>

			</div>
			<div class="four wide column"></div>
		</div>

	</div>
	<!--
	<br/>
	<br/>
	<p style="font-size:30px; margin-left:40px">Policies</p>
	<div style="margin-left:40px;width:1200px" class="ui blue compact inverted segment">
		<div class="ui inverted relaxed divided list">
			<div class="item" style=padding:12px>
				<p>Domains that are used as URL shorteners must include filters to block shortcuts to inappropriate content. Any violation of this policy will result in the immediate removal of the concerned domain.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Registrants with web sites that are discovered to be risky (i.e., contain browser exploits, phishing, excessive popups, trojans, sypware etc.), either directly by the TTNIC or through complaints from Internet users, will be given 24 hours to remove the offending code before their domain is taken out of service. A second violation could lead to the permanent deletion of the domain.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Registrations are processed on a first come first served basis but we abide by International copyright rules.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>We do not support Registrars and so all registrations must be made through the TTNIC.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Registrants are not restricted to have a local presence but those that do (i.e., have a local addresss) are provided discounted fees.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>All changes to registration details are free but we do not permit changes in the Registrant's name (except if a company has a name change)</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Domain names must include only letters, numbers, or hyphens ("-") and cannot have more than 63 characters. They cannot begin with a hyphen.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Subdomains within edu.tt are restricted to educational institutions within Trinidad and Tobago.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Applications for subdomains within gov.tt must be made directly with the Government of Trinidad and Tobago and hence must follow their policies.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>We reserve the right to remove domains that provide objectionable content and not to renew domains on expiration.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>We reserve the right to reject domain names that are deemed to be objectionable.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>Registrants must pay within 30 days of registration (or within 30 days of the expiration date in the case of renewals) else the domain may be deleted.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>The billing terms are provided on the invoice and pricing information is available at www.nic.tt.</p>
			</div>
			<div class="item" style=padding:12px>
				<p>or each registered domain, we make available (on our web site) the name of the Registrant, the address of the Registrant, the email addresses of Administrative, Billing and Technical Contacts, the corresponding nameserver information and the expiration date of the domain.</p>
			</div>
		</div>
	</div>
	</div>
	<div style="margin-left:40px;width:1200px" class="ui red compact inverted segment">
		<div class="ui inverted relaxed divided list">
			<div class="item">
				<div class="content">
					<div class="header">Privacy Policy</div>
					This policy covers how we use your personal information. We take your privacy seriously and will take all measures to protect your personal information. Any personal information received will only be used to fill your order. We will not sell or redistribute your information to anyone.
				</div>
			</div>
		</div>
	</div>
	<div style="margin-left:40px;width:1200px" class="ui red compact inverted segment">
		<div class="ui inverted relaxed divided list">
			<div class="item">
				<div class="content">
					<div class="header">Refund Policy</div>
					Our policy is not to offer refunds. This is due to costs, mostly administrative and Registry costs, incurred immediately upon registration of the domain name. This policy is also to prevent applicants from "domain tasting" and is a standard policy for ccTLD Registries.
				</div>
			</div>
		</div>
	</div>
	<p style="text-align:center">© Copyright 1995-2010 by TTNIC. All Rights Reserved.</p>
	-->
@stop
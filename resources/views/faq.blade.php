@extends('app')

@section('content')

<div class="ui segment">
<br/>
  <p style="font-size:20px; margin-left:40px">Anything you want to know?</p>
            
  <table class="ui purple celled collapsing table" style="margin-left:60px">
    <thead>
      <tr>
        <th class="two wide">Questions</th>
        <th>Answers</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="color:red">Can you explain how the DNS system works?</td>
        <td> A nicely written <a href="https://www.icann.org/en/system/files/files/domain-names-beginners-guide-06dec10-en.pdf">Beginner's Guide to Domain Names</a> is provided by <a href="https://www.icann.org">ICANN</a> .</td>      
      </tr>
      <tr>
        <td style="color:red">Do you accept credit card payments? </td>
        <td>Yes we accept credit card and Paypal payments through 2CheckOut. If you have outstanding payments, access your account balance and you will be provided with a link to make a credit card payment. The balance will include outstanding payments for new registrations and renewals of all domains for which you are the billing contact.</td>
      </tr>
      <tr>  
        <td style="color:red">Can I register a domain name prior to obtaining my nameservers? </td>
        <td>If you wish to reserve your domain name and do not yet have nameservers then please use "null.tt" for the first two hostnames and "0.0.0.0" for the first two IP addresses. We would then process your application and will await your new nameserver information before activating your domain.</td>
      </tr>
      <tr>	
        <td style="color:red">Can I change the name of the Registrant?</td>
        <td>Changes in the name of the registrant of a domain are allowed (without charge) only if the name of the company has changed or the company was bought by another company. In all other cases a new application must be submitted.</td>
        </tr>
      <tr>
        <td style="color:red"> How can I make a wire transfer to make my payment? </td>
        <td>Our account information for wire transfers is contained <a href="#">here</a>.</td>
      </tr> 
      <tr>
      	<td style="color:red">Do you provide web hosting or email addresses with a registration? </td>
      	<td>We are a DNS registry and so we only provide nameserver entries for your domain. If additional services are required then there are local companies that can assist such as <a href="http://www.forwardmultimedia.com">Forward Multimedia</a> .</td>
      </tr> 
      <tr>
      	<td style="color:red"> How can I add MX , A and other records for my domain? </td>
      	<td>All (except NS) records for your domain must be entered on your nameservers.</td>
      </tr>
      <tr>
      	<td style="color:red"> Why am I unable to register a domain of the form www.name.co.tt?</td>
      	<td>www.name.co.tt is a hostname and one should only apply for the domain name (in this case name.co.tt). You can always include records for sub-domains (e.g., dept.name.co.tt) and hostnames (e.g., www.name.co.tt) on your nameservers.</td>
      </tr>
      <tr>
      	<td style="color:red">Do you support Registrars?</td>
      	<td>At this point in time we do not support Registrars but that policy may change in the future. We do however offer special discount programs for our large customers. Please contact us for more details.</td>
      </tr>
      <tr>
      	<td style="color:red">Do you support DNSSEC? </td>
      	<td>We have deployed DNSSEC and so can now offer DNSSEC support for our subdomains. Please contact us if you would like us to add a DS recor</td>
      </tr>
      <tr>
      	<td style="color:red">Are your prices competitive? </td>
      	<td>ur third level domains cost $16.67US per year for local registrants (TT Addresses) which is comparable to the $14.99US charged by other registries for a .com address. Also, US-based Registries charge $20 US to $90 US per year for its third level North American country code domains while we charge foreign registrants $33US per year for third level domains. We surveyed the cost of second level domains over several Caribbean islands and found that we are quite price competitive on a per year cost. We charge $100US per year for local registrants and $200US per year for foreign registrants. Single letter domains are a very limited resource and hence are priced accordingly (contact us for further details).</td>
      </tr>
    </tbody>
  </table>
</div>

@stop
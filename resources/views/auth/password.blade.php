@extends('app')

@section('content')
	<div class="">
		<div class="">
			<div class="ui three column centered grid">
				<div class="column">
					<div class="column">
						@if (session('status'))
							<div class=""><!--Put in success css-->
								{{ session('status') }}
							</div>
						@endif

						@if (count($errors) > 0)
							<div class=""><!--Put in error css-->
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<form class="ui form" role="form" method="POST" action="/password/email">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="field">
								<label>E-Mail Address</label>
									<input type="email" class="" name="email" value="{{ old('email') }}" required>
							</div>
								<div class="field">
									<button type="submit" class="ui submit button">
										Send Password Reset Link
									</button>
								</div>
						</form>
					</div>
					<div class="column"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

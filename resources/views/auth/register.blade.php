@extends('app')

@section('content')
	<div class="">
		<div class="">
			<div class="ui three column centered grid">
				<div class="column">
					<div class="column">
						@if (count($errors) > 0)
							<div class=""><!--Put in error css-->
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<form class="ui form" role="form" method="POST" action="/auth/register">
							<h4 class="ui dividing header">Registration</h4>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="required field">
								<label>Name</label>
								<input type="text" name="name" value="{{ old('name')}}" required>
							</div>

							<div class="required field">
								<label>E-Mail Address</label>
								<input type="email" class="" name="email" value="{{ old('email') }}" required>
							</div>

							<div class="required field">
								<label>Password</label>
								<input type="password" class="form-control" name="password" required>
							</div>

							<div class="required field">
								<label>Confirm Password</label>
								<input type="password" class="" name="password_confirmation" required>
							</div>

							<div class="field">
								<button type="submit" class="ui submit button">
									Register
								</button>
							</div>

						</form>
					</div>
					<div class="column"></div>
				</div>
			</div>
		</div>
	</div>
@stop

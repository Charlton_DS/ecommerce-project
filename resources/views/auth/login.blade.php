@extends('app')

@section('content')
	<div class="">
		<div class="">
			<div class="ui three column centered grid">
				<div class="column">
					<div class="column">
						@if (count($errors) > 0)
							<div class="ui negative message"><!-- put in red-->
								<strong>Whoops!</strong> There were some problems with your input.
								<div class="ui bulleted list">
									@foreach ($errors->all() as $error)
										<div class="item"> {{$error}} </div>
									@endforeach
								</div>
							</div>
						@endif

						{!! Form::open(['action' => 'Auth\AuthController@postLogin', 'class' => 'ui form']) !!}
						<h4 class="ui dividing header">Login</h4>
						<div class="field">
							{!! Form::label('E-Mail Address') !!}
							{!! Form::text('email','',['class' => '', 'value' => old('email') , 'required']) !!}
						</div>

						<div class="field">
							{!! Form::label('Password') !!}
							{!! Form::password('password', '', ['required']) !!}
						</div>

						<div class="field">
							<div class="ui checkbox">
								{!! Form::checkbox('remember','')  !!}
								{!! Form::label('Remember Me') !!}
							</div>
						</div>

						<div class="ui hidden divider"></div>
								{!! Form::button('Login',['class'=> 'ui submit button', 'type'=> 'submit' ]) !!}
								<a href="/password/email">Forgot Your Password?</a>
						{!! Form::close() !!}
					</div>
					<div class="column"></div>
				</div>
			</div>
		</div>
	</div>
@endsection

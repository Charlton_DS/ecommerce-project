@extends('app')

@section('content')
<div class="">
	<div class="">
		<div class="">
			<div class="">
				<div class="">Reset Password</div>
				<div class="">
					@if (count($errors) > 0)
						<div class=""><!--Put in error css-->
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="" role="form" method="POST" action="/password/reset">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

						<div class="">
							<label class="">E-Mail Address</label>
							<div class="">
								<input type="email" class="" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="">
							<label class="">Password</label>
							<div class="">
								<input type="password" class="" name="password">
							</div>
						</div>

						<div class="">
							<label class="">Confirm Password</label>
							<div class="">
								<input type="password" class="" name="password_confirmation">
							</div>
						</div>

						<div class="">
							<div class="">
								<button type="submit" class="">
									Reset Password
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

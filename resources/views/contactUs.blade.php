@extends('app')
	@section('content')
  <div class="ui segment">
  <br/>
  	<p style="font-size:30px; margin-left:40px">Contact us when you want</p>
  <br/>
  <h4 class="ui blue header">Please use the most appropriate means of communication from the following:</h4>
   <div class="ui table">
     <table>
        <tbody>
   		<tr>
   			<td class="right aligned">Administrative Contact:</td>
   			<td><a href="mailto:admin@nic.tt">admin@nic.tt</a></td>
   		</tr>
   		<tr >
   			<td class="right aligned">Billing Contact:</td>
   			<td><a href="mailto:billing@nic.tt">billing@nic.tt</a></td>
   		</tr>
   		<tr>
   			<td class="right aligned">Technical Contact:</td>
   			<td><a href="mailto:tech@nic.tt">tech@nic.tt</a></td>
   		</tr>
   		<tr>
   			<td class="right aligned">Postal Address:</td>
   			<td>77 Abercromby St., Port of Spain, Trinidad, West Indies</td>
   		</tr>
   		<tr>
   			<td class="right aligned">TT Phone:</td>
   			<td><a href="tel:+18684834454">868 483-4454</a></td>
   		</tr>
   		<tr>
   			<td class="right aligned">US Phone:</td>
   			<td><a href="tel:+18582293651">858 229-3651</a></td>			
   		</tr>
   		<tr>
   			<td class="right aligned">Google Phone</td>
   			<td><a href="tel:+18584801403">858 480-1403</a></td>
   		</tr>
   		<tr>
   			<td class="right aligned">Fax:</td>
   			<td>253 423-9508</td>
   		</tr>
   		<tr>
   			<td class="right aligned">Office Location:</td>
   			<td>At the corner of Park and Abercromby Street, proceed north on Abercromby St.
The TTNIC office is in the 3rd building on the left from the corner.
The Office Telephone number is <a href="tel:+18686257092">1-868-625-7092</a>.</td>
   		</tr>
   		<tr>
   			<td class="right aligned">Office Directions:</td>
   			<td>
   			<div class="ui form"> 
   			<div class="inline field"><label>Enter your location</label>
   			<input type="text" placeholder="Street Address, City, Trinidad"> <div class="ui tiny blue animated button">
   			<div class="visible content">Get directions</div>
   			<div class="hidden content"><i class ="right arrow icon"></i>Search</div>
   			</div>
   			</div>
   			</div>
   			</td>
   		</tr>
   		</tbody>
   	  </table>	
   </div>
  </div>
@stop
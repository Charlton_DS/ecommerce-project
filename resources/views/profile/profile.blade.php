


<h3>Profile Information</h3>
	<div>
		<div class="ui label">
			Account Email
		</div>
	{{ $user->email }}
	</div>
	{!! Form::open(['action' => '']) !!}
	<div>
		<div class="ui label">
			Username
		</div>
	{!! Form::text('username',$user->name,['class' => '']) !!}
	</div>
	<div>
		<div class="ui label">
			Password
		</div>
	{!! Form::password('password','',['class' => '']) !!}
	</div>
	<div>
		<div class="ui label">
			New Password
		</div>
	{!! Form::password('newpassword','',['class' => '']) !!}
	</div>
	<div>
		<div class="ui label">
			Repeat Password
		</div>
	{!! Form::password('repeatpassword','',['class' => '']) !!}
	</div>
	{!! Form::close() !!}
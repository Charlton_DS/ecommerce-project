@extends('app')

@section('content')

    <div class="ui three column centered grid">

        <div class="column"></div>
        <div class="column">
            @if (count($errors) > 0)
                <div class="ui negative message"><!-- put in red-->
                    <strong>Whoops!</strong> Your phone number is not valid for that country.
                </div>
            @endif
            {!! Form::open(['action' => array('ProfileController@update',$user->id), 'method' => 'put', 'class' => 'ui form']) !!}
            <h4 class="ui dividing header">Your Information</h4>
            <div>
                <div class="field">
                    <label>Account Email:</label>
                    <div style="padding-top:10px; padding-bottom:15px">
                        {{ $user->email }}
                    </div>
                </div>
            </div>
            <div class="field">
                <label>Full Name</label>
                {!! Form::text('name',$user->name,['placeholder' => $user->name, 'required'])!!}
            </div>
            <div class="two fields">
                <div class="required field">
                    <label>Phone Contact</label>
                    {!! Form::text('phone',$user->phone,['placeholder' => 'Phone', 'required'])!!}
                </div>
                <div class="field">
                    <label>Country</label>
                    <select name="country" class="ui search dropdown" required>
                        @foreach(Countries::all() as $country)
                            @if($country->iso_3166_2== $user->country)
                                <option selected value={{$country->iso_3166_2}} ><i class="{{strtolower($country->iso_3166_2)}} flag"></i> {{$country->name}}</option>
                            @else
                                <option value={{$country->iso_3166_2}}><i class="{{strtolower($country->iso_3166_2)}} flag"></i> {{$country->name}}</option>
                            @endif
                        @endforeach
                    </select>


                </div>
            </div>
            <div class="required field">
                <label>Address</label>
                {!! Form::textArea('address',Auth::user()->address,['placeholder' => 'Business Address', 'required' => ''])!!}
            </div>
            <h4 class="ui dividing header"></h4>
            <div class="field">
                <button class="ui labeled positive icon button" type="submit">
                    <i class="checkmark icon"></i>
                    Submit
                </button>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="column"></div>

    </div>


@stop
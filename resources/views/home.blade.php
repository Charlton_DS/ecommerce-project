@extends('app')

@section('content')
	<!--{{$user->adminDomains()->get()}}-->

	<div class="ui grid">
		<div class="three column row">
			<div class="five wide column"></div>
			<div class="six wide column">
				@if(Auth::user()->phone == '5555555555' || Auth::user()->address == null )
					<div class="ui warning message">
						Please {!! HTML::link(action('ProfileController@edit'), 'edit') !!}
						your profile information before continuing.<br>
						This will help greatly with the registration process.
					</div>
				@endif
				@if($user->adminDomains()->count() > 0)
					<div class="ui label">
						Number of Registered Domains: {{$count = $user->adminDomains()->count()}}
					</div>
					@foreach($user->adminDomains()->get() as $domain)
						<div class="ui segments">
							@if($domain->activated)
								<div class="ui inverted green segment">
									<h3>{{$domain->name.'.'.$domain->tld}}</h3>
								</div>
							@else
								<div class="ui inverted orange segment">
									<h3>{{$domain->name.'.'.$domain->tld}}</h3>
								</div>
							@endif

							<div class="ui secondary segment">
								<p><b>Created: </b>{{$domain->created_at->diffForHumans()}}</p>
								<p><b>Expires: </b>{{$domain->expire->diffForHumans()}}</p>
								@if($domain->activated)
									<p><b>Active: </b>Yes</p>
								@else
									<p><b>Active: </b>No</p>
								@endif
							</div>
							@if(isset($domain->dns_hosts))
								<div class="ui secondary segment">
									<p><b>DNS Hostnames: </b>{{$domain->dns_hosts}}</p>
									<p><b>DNS Ips: </b>{{$domain->dns_ips}}</p>
								</div>
							@endif
							<div class="ui secondary segment">
								{!! Form::open(['action' => array('DomainsController@edit',$domain->id), 'method' => 'get']) !!}
								<button class="ui labeled positive icon button">
									<i class="edit icon"></i>
									Edit Domain
								</button>
								{!! Form::close() !!}

							</div>
						</div>
					@endforeach
				@else
					<div>
						<h2 class="ui center aligned icon header">
							<i class="info circle icon"></i>
							<div class="content">
								No Registered Domains
								<div class="sub header">Try searching for the domain you want</div>
							</div>
						</h2>
						<center>
							{{\Illuminate\Foundation\Inspiring::quote()}}
						</center>
					</div>
				@endif
			</div>

			<div class="five wide column"></div>
		</div>

	</div>
	</div>
	</div>
@endsection

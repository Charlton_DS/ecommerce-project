$('.ui.dropdown')
  .dropdown()
;

$('#search-select')
    .dropdown('hide others')
;

$('.ui.checkbox')
    .checkbox()
;

$('.ui.accordion')
    .accordion('open',0)
;

$('.item')
    .popup()
;

$('textarea')
    .popup()
;

$('.message .close').on('click', function() {
    $(this).closest('.message').fadeOut();
});

